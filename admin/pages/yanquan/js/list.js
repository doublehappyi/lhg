/**
 * Created by rt.yishuangxi on 2015/1/9.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    var IScrollMore = require("plugins/iscrollmore/iscrollmore");
    var yiDate = require("plugins/yiDate/yiDate");

    $(function () {
        list();
        sideBox();
    });
    function list() {
        var goodsSubject = utils.getHrefField("goodsSubject");
        var verifyCode = utils.getHrefField("verifyCode");
        var verifyTimeStart = utils.getHrefField("verifyTimeStart");
        var verifyTimeEnd = utils.getHrefField("verifyTimeEnd");

        var url = ctxPaths + "/maOrderVerify/queryVerifyCodeListWap.ajax?isVerify=00&";
        goodsSubject === "" ? "" : url += "goodsSubject=" + goodsSubject + "&";
        verifyCode === "" ? "" : url += "verifyCode=" + verifyCode + "&";
        verifyTimeStart === "" ? "" : url += "verifyTimeStart=" + verifyTimeStart + "&";
        verifyTimeEnd === "" ? "" : url += "verifyTimeEnd=" + verifyTimeEnd + "&";

        //去掉最后一个&符号
        url = url.replace(/&$/,"");
        var is = new IScrollMore({
            selector: ".j-iscroll-container",
            url: url,
            template: "list",
            first_success: function (data) {
                $(".list-count-container").show().find(".list-count").text(data.records);
            }
        });
    }

    function sideBox() {
        $(".side-box").click(function (e) {
            var container = $('.side-menu');
            if ((!container.is(e.target) && container.has(e.target).length === 0) &&
                (!($('.menu-icon').is(e.target)) && $('.menu-icon').has(e.target).length === 0)) {
                container.removeClass("in");
                $('body,.side-menu').removeClass("open");
                $('.side-box').hide();
            }
        });
        $(".menu-icon").click(function (e) {
            e.preventDefault();
            if ($('.side-menu, body').hasClass('open')) {
                $('.side-menu').removeClass('open');
                $('.side-box').hide();
                $('body').removeClass('open');
            }
            else {
                $('.side-menu').addClass('open');
                $('.side-box').show();
                $('body').addClass('open');
            }
        });

        startDate = new yiDate(".j-verifyTimeStart");

        endDate = new yiDate(".j-verifyTimeEnd");

        $(".j-list-search").click(function () {
            var goodsSubject = $(".j-goodsSubject").val();
            var verifyCode = $(".j-verifyCode").val();
            var verifyTimeStart = $(".j-verifyTimeStart").val();
            var verifyTimeEnd = $(".j-verifyTimeEnd").val();

            var url = ctxPaths + "/pages/yanquan/list.shtml?";
            goodsSubject === "" ? "" : url += "goodsSubject=" + encodeURIComponent(goodsSubject) + "&";
            verifyCode === "" ? "" : url += "verifyCode=" + encodeURIComponent(verifyCode) + "&";
            verifyTimeStart === "" ? "" : url += "verifyTimeStart=" + encodeURIComponent(verifyTimeStart) + "&";
            verifyTimeEnd === "" ? "" : url += "verifyTimeEnd=" + encodeURIComponent(verifyTimeEnd) + "&";

            //去掉最后一个&符号
            url = url.replace(/&$/,"");
            location = url;
        });
    }
});