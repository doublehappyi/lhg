/**
 * Created by rt.yishuangxi on 2015/1/9.
 */
define(function (require, exports, module) {
    var IScrollMore = require("plugins/iscrollmore/iscrollmore");

    $(function () {
        list();
    });
    function list() {
        var url = ctxPaths+"/maSmsPromotion/pageQueryByStoreId.ajax";
        var is = new IScrollMore({
            selector: ".j-iscroll-container",
            url: url,
            template: "list",
            first_success: function (data) {
                //$(".list-count-container").show().find(".list-count").text(data.records);
            }
        },{click:true});
    }
});