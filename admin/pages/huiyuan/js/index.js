/**
 * Created by rt.yishuangxi on 2015/1/12.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    var IScrollMore = require("plugins/iscrollmore/iscrollmore");

    $(function () {
        list();
        bindEvents();
    });
    function list() {
        var url = ctxPaths + "/maStoreMember/pageQueryByStoreId.ajax?";
        var url = sortTable(url, utils.getHrefField("orderBy"), utils.getHrefField("sort"));

        var is = new IScrollMore({
            selector: ".j-iscroll-container",
            url: url,
            template: "list",
            first_success: function (data) {
                $(".list-count-container").show().find(".list-count").text(data.records);
            }
        }, {click: true});
    }

    function bindEvents() {
        var url = ctxPaths + "/pages/huiyuan/index.shtml?";
        var $orderSet = $(".j-order-set");
        var orderBy, sort;
        /*$('#element').on('tap', doSomething); */
        $orderSet.click(function () {
            var $this = $(this);
            var $sort = $this.find(".table-sort");
            var sort = $sort.attr("data-sort");

            orderBy = $sort.attr("data-orderBy");

            if (sort === "asc") {
                sort = "desc";
            } else if (sort === "desc") {
                sort = "asc";
            } else {
                orderBy = $sort.attr("data-orderBy");
                sort = "asc";
            }
            url += "orderBy=" + orderBy + "&" + "sort=" + sort;
            location = url;
        });
    }

    //&darr;降序
    //&uarr;升序
    function sortTable(url, orderBy, sort) {
        if (orderBy && sort) {
            var orderBy = orderBy;
            var sort = sort;
        } else {
            var orderBy = "maStoreMemberNo";
            var sort = "desc";
        }

        var orderByArrays = ["maStoreMemberNo", "orderTimes", "totalYuan", "careTime"];
        var sortArrays = ["asc", "desc"];

        if (orderByArrays.indexOf(orderBy) !== -1 && sortArrays.indexOf(sort) !== -1) {
            url += "orderBy=" + orderBy + "&" + "sort=" + sort;
            $(".table-sort").text("");
            /*$(".j-orderTimes").text("");
             $(".j-totalYuan").text("");*/
            if (sort === sortArrays[0]) {
                $(".j-" + orderBy).html("&uarr;").attr("data-sort", sort);
                ;
            } else {
                $(".j-" + orderBy).html("&darr;").attr("data-sort", sort);
            }
        } else {
            url += "orderBy=" + orderBy + "&" + "sort=" + sort;
            $(".j-maStoreMemberNo").html("&darr;");
        }

        return url;
    }
});