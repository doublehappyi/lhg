/**
 * Created by rt.yishuangxi on 2015/1/12.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    var yiLayer = require("plugins/yiLayer/yiLayer.js");
    $(function () {
        dxyx();
        bindEvents();
    });

    function dxyx() {
        var url = ctxPaths + "/maStoreMember/getNumByStoreId.ajax";
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    $(".memberNum").text(data.data.memberNum);
                }
            }
        });

        var maSmsPromotionId = utils.getHrefField("MaSmsPromotionId");
        if (maSmsPromotionId) {
            var sms_url = ctxPaths + "/maSmsPromotion/get.ajax";
            $.ajax({
                url: sms_url,
                data:{
                    maSmsPromotionId:maSmsPromotionId
                },
                success: function (data) {
                    if (data.success) {
                        $(".Content").val(data.data.content);
                    }
                }
            });
        }
    }

    function bindEvents(){
        var $submit = $(".submit");
        var $content = $(".Content");

        $content.keyup(function () {
            var text_count = 70;
            var $this = $(this);
            var val = $this.val();
            if (val === "" || val.length > text_count) {
                $submit.attr("disabled", "true").addClass("btn_g");
            } else {
                $submit.attr("disabled", null).removeClass("btn_g");
            }
            $("#letter-left").text(text_count - val.length);
        });

        $submit.click(function () {
            var url = ctxPaths + "/maSmsPromotion/save.ajax";
            var Content = $(".Content").val();

            var data = {
                Content: Content
            };
            var MaSmsPromotionId = utils.getHrefField("MaSmsPromotionId");
            if (MaSmsPromotionId) {
                data.MaSmsPromotionId = MaSmsPromotionId;

            }
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (data) {
                    if (data.success) {
                        var successLayer = new yiLayer("#yiLayer-success");
                        $(successLayer.selector).find("p").text(data.message);
                        successLayer.show();
                    } else {
                        var errorLayer = new yiLayer("#yiLayer-error");
                        $(errorLayer.selector).find("p").text(data.message);
                        errorLayer.show();
                    }

                }
            });
        });
    }
});