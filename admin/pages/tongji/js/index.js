/**
 * Created by rt.yishuangxi on 2015/1/12.
 */
define(function (require, exports, module) {
    var template = require("template");
    $(function () {
        renderComment();
        renderChart();
    });

    function renderComment() {
        var url = ctxPaths + "/maStore/selectByPrimaryKey.ajax";
        $.ajax({
            url: url,
            success: function (data) {
                var data = data.rows[0];
                var totalScore = template("totalScore", data);
                var serviceScore = template("serviceScore", data);
                var conditionScore = template("conditionScore", data);

                $(".totalScore").html(totalScore);
                $(".serviceScore").html(serviceScore);
                $(".conditionScore").html(conditionScore);

                $("#fenlei").text(data.storeCateId);


                if (data.totalAvg == "0%") {
                    $("#totalAvg").text("持平");
                    $("#totalPercent").text("--");
                } else {
                    if (data.totalAvg.indexOf("-") < 0) {
                        $("#totalAvg").text("高于");
                        $("#totalPercent").text(data.totalAvg);
                    } else {
                        $("#totalAvg").text("低于");
                        $("#totalPercent").text(data.totalAvg.substring(1));
                    }
                }

                if (data.conditionAvg == "0%") {
                    $("#conditionAvg").text("持平");
                    $("#conditionPercent").text("--");
                } else {
                    if (data.conditionAvg.indexOf("-") < 0) {
                        $("#conditionAvg").text("高于");
                        $("#conditionPercent").text(data.conditionAvg);
                    } else {
                        $("#conditionAvg").text("低于");
                        $("#conditionPercent").text(data.conditionAvg.substring(1));
                    }
                }

                if (data.serviceAvg == "0%") {
                    $("#serviceAvg").text("持平");
                    $("#servicePercent").text("--");
                } else {
                    if (data.serviceAvg.indexOf("-") < 0) {
                        $("#serviceAvg").text("高于");
                        $("#servicePercent").text(data.serviceAvg);
                    } else {
                        $("#serviceAvg").text("低于");
                        $("#servicePercent").text(data.serviceAvg.substring(1));
                    }
                }
            }
        })
    }

    function renderChart() {
    	//折线统计图形。。。。。。
		
        //声明报表对象  
    var chart = new Highcharts.Chart({  
        chart: {  
            //将报表对象渲染到层上  
        renderTo: 'container'  
    },  
    title: {
        text: '会员统计图',
        x: -20 //center
	},
	//去掉右下角的highcharts.com
	credits: {
		 enabled:false 
	},
	xAxis: {
    	categories: ['1日', '2日', '3日', '4日', '5日', '6日','7日']
		},
	yAxis: {
        title: {
            text: '数量'
        }
	},
    //设定报表对象的初始数据  
    series: [{
    	name: '新增会员',  
        data: [0, 0, 0, 0, 0, 0 ,0]
    },{  
    	name: '消费用户',
        data: [0, 0, 0, 0, 0, 0 ,0]
    }
    ]
});  

	$.ajax({
		type: "GET",
		url: ctxPaths + '/maStoreMember/getMembershipCount.ajax',
		dataType: "json",
		success: function(data){
				//日头反转
				var arr = new Array();
				for(var i = 0;i < 7 ; i++){
					arr[i] = (data.rows[i].memberDay);
				}
				arr.reverse();
				
				//日   新增会员反转
				var addMember = new Array();
				for(var i = 0;i < 7 ; i++){
					addMember[i] = (data.rows[i].memberDayCount);
				}
				addMember.reverse();
				
				//日   消费用户反转
				var orderUser = new Array();
				for(var i = 0;i < 7 ; i++){
					orderUser[i] = (data.rows[i].orderDayCount);
				}
				orderUser.reverse();
				
				//月头反转
				var arrMonth = new Array();
				for(var i = 7;i < 13 ; i++){
					arrMonth[i] = (data.rows[i].memberMonth);
				}
				arrMonth.reverse();
				
				//月  新增会员反转
				var addMemberTwo = new Array();
				for(var i = 7;i < 13 ; i++){
					addMemberTwo[i] = (data.rows[i].memberMonthCount);
				}
				addMemberTwo.reverse();
				
				//月  消费用户反转
				var orderUserTwo = new Array();
				for(var i = 7;i < 13 ; i++){
					orderUserTwo[i] = (data.rows[i].orderMonthCount);
				}
				orderUserTwo.reverse();
				
				
				allCount();
			
			$('#btnDay').click(function (){
				
				allCount();
				
			});
			
			$('#btnMonth').click(function(){
				
				//会员月
				chart.series[0].setData([addMemberTwo[0], addMemberTwo[1], addMemberTwo[2], addMemberTwo[3], addMemberTwo[4], addMemberTwo[5]]);
				
				//消费月
				chart.series[1].setData([orderUserTwo[0], orderUserTwo[1], orderUserTwo[2], orderUserTwo[3], orderUserTwo[4], orderUserTwo[5]]);
				//头
				chart.xAxis[0].setCategories([''+arrMonth[0]+'月', ''+arrMonth[1]+'月', ''+arrMonth[2]+'月', ''+arrMonth[3]+'月', ''+arrMonth[4]+'月', ''+arrMonth[5]+'月'],true);
				
				for(var i = 1 ;i < 7;i++){
					//如果是显示月     显示列表头
					$('#DayMonth'+i).html(arrMonth[i-1]+"月");//显示列表头
					//新增会员
					$('#memberCount'+i).html(addMemberTwo[i-1]);
					//消费用户
					$('#orderCount'+i).html(orderUserTwo[i-1]);
					
					$('#dis').hide();$('#disa').hide();$('#disas').hide();
				}
				
			});
			
			function allCount(){
				//如果显示日
				//会员日
				chart.series[0].setData([addMember[0], addMember[1], addMember[2], addMember[3], addMember[4], addMember[5],addMember[6]]); 
		
				//消费用户日
				chart.series[1].setData([orderUser[0], orderUser[1], orderUser[2], orderUser[3], orderUser[4], orderUser[5],orderUser[6]]); 
			
				//头
				chart.xAxis[0].setCategories([''+arr[0]+'日', ''+arr[1]+'日', ''+arr[2]+'日', ''+arr[3]+'日', ''+arr[4]+'日', ''+arr[5]+'日',''+arr[6]+'日'],true);
			
				for(var i = 1 ;i < 8;i++){
					//如果是显示月     显示列表头
					$('#DayMonth'+i).html(arr[i-1]+"日");//显示列表头
					//新增会员
					$('#memberCount'+i).html(addMember[-1+i]);
					//消费用户
					$('#orderCount'+i).html(orderUser[-1+i]);
					
					$('#dis').show();$('#disa').show();$('#disas').show();
				}
			}
			
				
		}
	});
    }
});