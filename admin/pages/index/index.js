define(function (require, exports, module) {
    var RSAKey = require("plugins/crypt/rsa");
    $(document).ready(function () {
        var yanQuanUrl = ctxPaths + '/pages/yanquan/index.shtml';
        var loginUrl = adminPath + "/portal/login.ajax";
        $("#loginButton").bind("click", loginAjax);

        $("#userpass").bind('focus', function () {
            $('#userpass').val('');
        });

        var errorTip = function (errorMsg) {
            $('#errorDisplayArea').text(errorMsg);
            if ('' == errorMsg) {
                $('#errorDisplayArea').hide();
            } else {
                $('#errorDisplayArea').show();
            }
        };

        function loginAjax() {
            var rsa = new RSAKey();
            try {
                rsa.setPublic(modulus, exponent);
                var passwordDe = rsa.encrypt($("#userpass").val());

                if ($("#isAuto")[0].checked) {

                    addCookie("isAuto", $("#isAuto").val(), 7, "/");
                } else {

                    deleteCookie("isAuto", "/");
                }
            } catch (e) {
                errorTip('登录失败，原因未知');
                return;
            }
            $("#password").val(passwordDe);
            $("#userpass").val(new Date().getTime());
            $('#loginButton').attr("disabled", true);

            $.ajax({
                type: "POST",
                url: loginUrl,
                dataType: "json",

                data: $('#loginForm').serialize(),
                beforeSend: function () {

                },
                success: function (data, status) {
                    if (data.success === true) {
                        errorTip('');
                        var ticket = getCookieValue("AUTH_TICKET");
                        window.location.href = yanQuanUrl + "?ticket=" + ticket;

                    } else {
                        errorTip(data.message);
                    }
                },
                error: function (jqXHR, errorMsg) {
                    errorTip('登录失败，原因未知');
                },
                complete: function () {
                    $('#loginButton').removeAttr("disabled");
                }

            });
        }

        var startLoad = function () {

            $("#loginName").val(getCookieValue("loginName"));
            $("#userpass").val(getCookieValue("userpass"));
            if (getCookieValue("isAuto") == "true") {

                window.location.href = yanQuanUrl
            }

        };

        function addCookie(name, value, days, path) {
            var name = encodeURIComponent(name);
            var value = encodeURIComponent(value);
            var expires = new Date();
            expires.setTime(expires.getTime() + days * 3600000 * 24);
            path = path == "" ? "" : ";path=" + path;
            var _expires = (typeof days) == "string" ? "" : ";expires=" + expires.toUTCString();
            document.cookie = name + "=" + value + _expires + path;
        }

        function getCookieValue(name) {
            var name = encodeURIComponent(name);
            var allcookies = document.cookie;
//        alert(allcookies)
            name += "=";
            var pos = allcookies.lastIndexOf(name);
            if (pos != -1) {
                var start = pos + name.length;
                var end = allcookies.indexOf(";", start);
                if (end == -1) end = allcookies.length;
                var value = allcookies.substring(start, end);
                return (value);
            } else {
                return "";
            }
        }

        function deleteCookie(name, path) {
            var name = encodeURIComponent(name);
            var expires = new Date(0);
            path = path == "" ? "" : ";path=" + path;
            document.cookie = name + "=" + ";expires=" + expires.toUTCString() + path;
        }

        function clearCookie() {
            var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
            if (keys) {
                for (var i = keys.length; i--;)
                    document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString()
            }
        }

        startLoad();

    })
});