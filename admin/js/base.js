//清空搜索框
$('.yq-list').children(':text').focus(function(){
	if(!this.initValue){
		this.initValue = this.value;
	}
	if(this.value === this.initValue){
		this.value = '';
		$(this).addClass("curr")
	}
}).blur(function(){
	if(this.value === '' || this.value === null){
		this.value = this.initValue;
		$(this).removeClass("curr")
	}
});


//显示验券结果
$(".yq-list .btn").click(function(){
	$(".yq-tips").show()
});
