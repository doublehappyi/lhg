/**
 * Created by rt.yishuangxi on 2015/1/10.
 */
define(function (require, exports, module) {
    /***
     * 函数功能:获取location.href中的某个指定字段的值，如果没有这个字段，则返回空字符串
     * @param field ：location.href中包含字段
     * @param href ： 可选参数，没有传入则默认使用location.href获取
     * @returns {string} ：返回值类型为字符串
     */
    exports.getHrefField = function (field, href) {
        var ret = "";
        var href = href || location.href;
        var fieldStr, fieldArr, index;
        if (href.indexOf("?") !== -1) {
            fieldStr = href.substring(href.indexOf("?") + 1);
            fieldArr = fieldStr.split("&");
            for (var i = 0, len = fieldArr.length; i < len; i++) {
                index = fieldArr[i].indexOf(field + "=");
                if (index !== -1) {
                    ret = fieldArr[i].substring(index + (field + "=").length);
                    break;
                }
            }

            return decodeURIComponent(ret);
        } else {
            return decodeURIComponent(ret);
        }
    };


    /****
     * 获取元素的占位高度
     * @param $dom 一定要是jquery对象
     * @returns {*}
     */
    exports.getRealHeight = function ($dom) {
        var h = $dom.height() +
            parseInt($dom.css("padding-top")) +
            parseInt($dom.css("padding-bottom")) +
            parseInt($dom.css("margin-top")) +
            parseInt($dom.css("margin-bottom")) +
            parseInt($dom.css("border-top-width")) +
            parseInt($dom.css("border-bottom-width"));
        return h;
    };

    /***
     * 获取元素的占位宽度
     * @param $dom 一定要是jquery对象
     * @returns {*}
     */
    exports.getRealWidth = function ($dom) {
        var w = $dom.width() +
            parseInt($dom.css("padding-left")) +
            parseInt($dom.css("padding-right")) +
            parseInt($dom.css("margin-left")) +
            parseInt($dom.css("margin-right")) +
            parseInt($dom.css("border-left-width")) +
            parseInt($dom.css("border-right-width"));
        return w;
    };
});