/**
 * Created by rt.yishuangxi on 2015/1/14.
 */
define(function(require, exports, module){
    if(document.querySelector(".history-back")){
        document.querySelector(".history-back").addEventListener("click", function(){
            history.back();
        },false);
    }

    $(document).ajaxComplete(function (event, xhr, settings) {
        try {
            var result = $.parseJSON(xhr.responseText);
            // 用户会话失效或未登录，重定向至登录页面
            if (result && result['redirectUrl']) {
                window.location.replace(result['redirectUrl']);
            }
        } catch (e) {
        }
    });


    $(document).ready(function () {
        var logoutAdminUrl = adminPath + "/portal/logout.ajax";
        var myLogoutUrl = ctxPaths + "/wapShop/logout.ajax";
        var ticket = getUrlParam("ticket");
        var yanQuanUrl = 'maOrderVerify/chkVerifyCodeWap.ajax';//+'?ticket='+ticket;
        $("#btn").bind("click", checkAjax);
        $("#logout").bind("click", logout);

        $("#passcode").focus(function () {


            this.value = '';
            $(this).addClass("curr")

        }).blur(function () {

            $(this).removeClass("curr")

        });

        function judgelength(val) {

            if (val.length === 10) {
                return true;
            }
            else {
                return false;
            }
        }

        function isNum(val) {
            if ((isNaN(val)) || (val.indexOf(".") != -1) || (val.indexOf("-") != -1))
                return false
            return true
        }


        var logoutMy = function () {
            $.ajax(
                {
                    type: "POST",
                    url: myLogoutUrl,
                    dataType: "json",
                    data: '',
                    beforeSend: function () {
                    },
                    success: function (data, status) {
                        if (data.success === true) {
                        } else {
                        }
                    },
                    error: function (jqXHR, errorMsg) {
                    },
                    complete: function () {
                        window.location = ctxPaths + "/pages/index/index.shtml"
                    }

                });
        }

        function logout() {
            $.ajax(
                {
                    type: "POST",
                    url: logoutAdminUrl,
                    dataType: "json",
                    data: '',
                    beforeSend: function () {
                    },
                    success: function (data, status) {
                        if (data.success === true || data.returnCode == "3") {
                            logoutMy();
                        } else {
                            return;
                        }
                    },
                    error: function (jqXHR, errorMsg) {
                    },
                    complete: function () {
                    }

                });
        }

        function checkAjax() {
            $("#chksuccess").css("display", "none");
            $("#chkfail").css("display", "none");
            $("#pzm2desc").css("display", "block");
            $('#errorDisplayArea').text('');

            var val = $("#passcode").val();
            if (judgelength(val) !== true || isNum(val) !== true) {
                $('#pzm2').text($("#passcode").val());
                $('#errortip').text("输入必须是10位数字");
                $("#chkfail").css("display", "block");
                $("#pzm2desc").css("display", "none");
                return;
            }
            ;
            $.ajax(
                {
                    type: "POST",
                    url: yanQuanUrl,
                    dataType: "json",
                    data: $.param({verifyCode: $("#passcode").val()}),
                    beforeSend: function () {

                    },
                    success: function (data, status) {
                        if (data.returnCode == "3") {
                            window.location.href = data.redirectUrl;
                            return;
                        }
                        if (data.success === true) {
                            if (data.data.isVerifyScuess === 'Y') {
                                $('#pzm').text(data.data.verifyCode);
                                $('#spmc').text(data.data.goodsSubject);
                                $('#spjk').text(data.data.goodsPrice + '元');
                                $('#xfsj').text(data.data.verifyTime);
                                $("#chksuccess").css("display", "block");
                                $("#chkfail").css("display", "none");
                                $("#passcode").val('');
                                $("#passcode").focus();

                            } else {
                                $('#pzm2').text(data.data.verifyCode);
                                $('#errortip').text(data.data.verifyFailMsg);

                                $("#chksuccess").css("display", "none");
                                $("#chkfail").css("display", "block");

                            }
                        } else {
                            $('#pzm2').text($("#passcode").val());
                            $('#errortip').text(data.message);
                            if ($("#passcode").val() == '') {
                                $("#pzm2desc").css("display", "none");
                            }
                            $("#chksuccess").css("display", "none");
                            $("#chkfail").css("display", "block");
                        }


                    },
                    error: function (jqXHR, errorMsg) {
                        $('#pzm2').text($("#passcode").val());
                        $('#errortip').text("未知异常");

                        $("#chksuccess").css("display", "none");
                        $("#chkfail").css("display", "block");
                        if ($("#passcode").val() == '') {
                            $("#pzm2desc").css("display", "none");
                        }
                    },
                    complete: function () {
                        $("#passcode").focus();
                    }

                });
        }
    })

    function getUrlParam(name) {

        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");

        var r = window.location.search.substr(1).match(reg);

        if (r != null) return unescape(r[2]);
        return null;

    }
});