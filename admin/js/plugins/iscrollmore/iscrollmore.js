/**
 * Created by rt.yishuangxi on 2015/1/9.
 */
define(function (require, exports, module) {
    var IScroll = require("plugins/iscroll/iscroll");
    var utils = require("utils");
    var template = require("template");

    //iscroll的配置，必传
    function IScrollMore(options,config) {
        var defaults = {
            selector: "",//iscroll的第一个参数，元素选择器，必传
            template: "",//模板id，必传
            url: "",//请求地址，必传
            page: 0,
            total: 1,
            first_success: function (data) {

            },
            success: function () {
            },
            error: function () {
            }
        };


        var options = $.extend(defaults, options);
        var is = new IScroll(options.selector, config);

        initIScroll(is, options);
        loadData(is, options);
        bindEvents(is, options);

        return is;
    }

    function loadData(is, options) {
        console.log("loading data...");
        is.$more.text("正在努力加载，请稍后...");
        $.ajax({
            url: options.url,
            data: {
                pageNo: options.page + 1
            },
            success: function (data) {
                if (data.success) {
                    if (data.page <= data.total && data.total !== 0) {//正常请求下来的数据
                        if (options.first_success) {//执行第一次请求成功的回调函数
                            options.first_success(data);
                            options.first_success = false;
                        }
                        options.success();
                        var html = template(options.template, data);
                        is.$more.before(html);
                        is.$more.text("下拉加载更多...");
                        options.page = data.page;
                        options.total = data.total;
                        //如果已经是最后一页
                        if (data.page === data.total) {
                            is.$more.remove();
                        }
                    } else if (data.total === 0) {//加载数据为空
                        is.$more.text("对不起，您还没有任何数据...");
                    }
                    refreshIscroll(is);
                } else {
                    options.error();
                }
            },
            complete: function () {
                console.log("ajax complete...");
                options.loading = false;//加载完成，把loading值置为false
            }
        });
    }

    function initIScroll(is, options) {
        is.$container = $(options.selector);
        is.$content = is.$container.children().first();
        if (is.$container.find(".add_more").length === 0) {
            is.$content.append('<div class="add_more">正在努力加载，请稍后...</div>');
            is.$more = is.$container.find(".add_more");
        }
        refreshIscroll(is);
    }

    function refreshIscroll(is) {
        is.$container.height($(window).height() - utils.getRealHeight($(".header")) - utils.getRealHeight($(".bottom_menu")));
        is.refresh();
    }

    function bindEvents(is, options) {
        var flag = false;
        is.on("scrollStart", function () {
            //console.log("is.y: ", is.y, " is.maxScrollY: ", is.maxScrollY);
            if (is.y === is.maxScrollY && is.directionY == 1) {
                flag = true;
            }
        });
        is.on("scrollEnd", function () {
            //必须同时满足3个条件才可以继续加载：1，已经在底部了，2，上一次数据加载已经完成，3，不是最后一页
            if (flag && !options.loading && options.page !== options.total) {
                options.loading = true;//加载开始，把loading值置为true，阻止重复加载
                flag = false;
                loadData(is, options);
            }
        });

        window.onresize = function () {
            refreshIscroll(is);
        }
    }

    module.exports = IScrollMore;
});