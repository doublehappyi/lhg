/**
 * Created by rt.yishuangxi on 2015/1/15.
 */
define(function (require, exports, module) {
    var yiLayer = require("plugins/yiLayer/yiLayer");
    require("plugins/seajs-css/seajs-css");
    seajs.use("plugins/yiDate/yiDate.css");

    var layerIdNum = 0;

    function yiDate(selector, options) {
        layerIdNum++;
        var layerId = "layer-id-" + layerIdNum;
        var defaults = {
            selector: "",
            year: 2015,
            month: 1,
            day: 1
        };

        this.html = '<div id="' + layerId + '" class="yiLayer yiDate" style=" height: auto">' +
        '<div class="yd-title">时间选择</div>' +
        '<div class="yd-select">' +
        '<div class="yd-ctn">' +
        '<div>年</div>' +
        '<i class="yd-plus" limit="">+</i>' +
        '<div class="yd-y">2014</div>' +
        '<i class="yd-minus">-</i>' +
        '</div>' +
        '<div class="yd-ctn">' +
        '<div>月</div>' +
        '<i class="yd-plus">+</i>' +
        '<div class="yd-m">0</div>' +
        '<i class="yd-minus">-</i>' +
        '</div>' +
        '<div class="yd-ctn">' +
        '<div>日</div>' +
        '<i class="yd-plus">+</i>' +
        '<div class="yd-d">0</div>' +
        '<i class="yd-minus">-</i>' +
        '</div>' +
        '</div>' +
        '<div class="yd-btn-ctn">' +
        ' <button class="yd-btn yd-btn-yes">确定</button>' +
        '<button class="yd-btn yd-btn-no yiLayer-close">取消</button>' +
        '</div>' +
        '</div>';

        $("body").append(this.html);
        this.options = $.extend({}, defaults, options);
        this.selector = selector;
        this.$target = $(selector);
        this.layer = new yiLayer("#" + layerId);
        this.$layer = $(this.layer.selector);

        this.init();
    }


    yiDate.prototype = {
        constructor: yiDate,
        init: function () {
            var self = this;
            this.$target.attr("readonly",true);
            this.$layer.find(".yd-y").text(self.options.year);
            this.$layer.find(".yd-m").text(self.options.month);
            this.$layer.find(".yd-d").text(self.options.day);

            this.bindEvents();

            /*$(window).trigger("resize");*/
        },
        bindEvents: function () {
            var self = this;
            self.$target.focus(function () {
                console.log(self.$target);
                $(window).trigger("resize");
                self.layer.show();

            });

            this.$layer.find(".yd-plus").click(function () {
                var $this = $(this);
                var $next = $this.next();
                var val = parseInt($next.text());
                if ($next.hasClass("yd-y")) {
                    $next.text(val + 1);
                } else if ($this.next().hasClass("yd-m")) {
                    if (val < 12) {
                        $next.text(val + 1);
                        self.$layer.find(".yd-d").text(1);
                    }
                } else if ($next.hasClass("yd-d")) {
                    var date = new Date(self.$layer.find(".yd-y").text(), self.$layer.find(".yd-m").text(), 0);
                    if (val < date.getDate()) {
                        $next.text(val + 1);
                    }

                } else {
                }
            });

            this.$layer.find(".yd-minus").click(function () {
                var $this = $(this);
                var $prev = $this.prev();
                var val = parseInt($prev.text());
                if (val > 1) {
                    $prev.text(val - 1);
                }

            });


            this.$layer.find(".yd-btn-yes").click(function () {
                var d = self.$layer.find(".yd-y").text() + "-" + self.$layer.find(".yd-m").text() + "-" + self.$layer.find(".yd-d").text();
                self.$target.val(d);
                self.layer.hide();
            });
        }
    };

    module.exports = yiDate;
});