/**
 * Created by rt.yishuangxi on 2014/12/24.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    require("plugins/seajs-css/seajs-css");
    //依赖yiLayer.css
    seajs.use("plugins/yiLayer/yiLayer.css");

    function yiLayer(selector, options) {
        var self = this;
        this.$layer = $(selector);
        this.selector = selector;
        this.options = $.extend({}, self.defaults, options);

        this._init();
    }

    yiLayer.prototype = {
        constructor: yiLayer,
        defaults: {
            masked: true,//是否显示遮罩
            closed: true,//是否显示弹层
            speed: 300//动画速度
        },
        $mask: $(".yiLayer-mask").length > 0 ? $(".yiLayer-mask") : $("<div class='yiLayer-mask'></div>"),
        _init: function () {
            this._initMask();
            this._initLayer();
            this._bindEvents();
        },
        show: function () {
            this._centerLayer();
            this.$layer.show();
            if (this.options.masked) {
                this.$mask.show();
            }
        },
        hide: function () {
            this.$layer.hide();
            if (this.options.masked) {
                this.$mask.hide();
            }

        },
        _centerLayer:function(){
            var self = this;
            /*console.log(" utils.getRealWidth(self.$layer): ", utils.getRealWidth(self.$layer));
            console.log("$(window).width(): ",$(window).width());*/
            this.left = ($(window).width() - utils.getRealWidth(self.$layer)) / 2;
            this.top = ($(window).height() - utils.getRealHeight(self.$layer)) / 2 + $(document).scrollTop();
            this.$layer.css({
                left: self.left,
                top: self.top
            });
        },
        _initMask: function () {
            var self = this;
            this.$mask.height($(document).height()).appendTo('body');
            //由于页面内容是ajax加载，页面高度随
            setInterval(function () {
                self.$mask.height($(document).height());
            }, 500);
            if (this.options.masked && !this.options.closed) {
                this.$mask.show();
            } else {
                this.$mask.hide();
            }
        },
        _initLayer: function () {
            this._centerLayer();
            if (this.options.closed) {
                this.$layer.hide();
            }
        },
        _bindEvents: function () {
            var self = this;
            //如果使用window.onresize来绑定处理函数，则后绑定的处理函数，会覆盖前面绑定的处理函数
            //所以这里一定要使用$(window).resize来绑定处理函数
            $(window).resize(function () {
                self.$mask.height($(document).height());
                self._centerLayer();
            });
            this.$mask.click(function () {
                self.hide();
            });

            //只要在容器this.$layer内部使用了类名.yiLayer-close的，则都可以点击关闭弹层
            this.$layer.find(".yiLayer-close").click(function () {
                self.hide();
            });
        }
    };

    module.exports = yiLayer;
});