/**
 * Created by rt.yishuangxi on 2014/11/11.
 */
(function () {
    /**
     * 插件依赖:该插件依赖jquery，以及yiScore.css和特定的html结构（yi.Score.html有demo，以及star.png图片。
     * 插件功能：提供评分功能，选择分数，并且点击就可以选中该分数，分数存储在调用该插件的dom的自定义属性data-score中
     * @param options 目前只定了回调函数参数，默认值为空函数
     * @returns {*}
     */
    $.fn.yiScore = function (options) {
        var defaults = {
            callback:function(){}
        }
        var options = $.extend({}, defaults, options);
        return this.each(function () {
            var $this = $(this),
                $star = $this.find(".score-star"),
                $scoreText = $this.find(".score-text");
            //初始化一些数据
            $scoreText.text("请评分");
            $this.attr("data-score", 0);//data-score属性用于存放点评之后的值，默认值为0

            //鼠标悬浮上来的时候，当前star以及排在其前面的star都要亮起来，而排在其后面的都要灰掉
            $star.hover(function () {
                var beforeThis = true;
                for (var i = 0; i < $star.length; i++) {
                    if ($star[i] !== this && beforeThis) {
                        $($star[i]).addClass("score-star-active");
                    } else if ($star[i] == this) {
                        $($star[i]).addClass("score-star-active");
                        $scoreText.text(i + 1 + "分").addClass("score-text-active");
                        beforeThis = false;
                    } else {
                        $($star[i]).removeClass("score-star-active");
                    }
                }
            });

            //鼠标离开整个$this的时候，只需要记住之前被点击过的位置及排在其前面的亮起来，后面的灰掉
            $this.hover(function () {
            }, function () {
                var scoreSelected = $this.find(".score-selected")[0];
                if (!scoreSelected) {
                    $star.removeClass("score-star-active");
                    $scoreText.text("请评分").removeClass("score-text-active");
                    return;
                }
                var beforeSelected = true;

                for (var i = 0; i < $star.length; i++) {
                    if ($star[i] !== scoreSelected && beforeSelected) {
                        $($star[i]).addClass("score-star-active");
                    } else if ($star[i] == scoreSelected) {
                        beforeSelected = false;
                        $($star[i]).addClass("score-star-active");
                        $scoreText.text(i + 1 + "分").addClass("score-text-active");
                    } else {
                        $($star[i]).removeClass("score-star-active");
                    }
                }
            });

            //点击star之后，去掉其他的star该标记，给当前star添加标记
            $star.click(function () {
                var selected = $this.find(".score-selected");
                if (selected.length > 0) {
                    selected.removeClass("score-selected");
                }
                var $scoreSelected = $(this).addClass("score-selected");
                for (var i = 0; i < $star.length; i++) {
                    if ($star[i] == $scoreSelected[0]) {
                        $this.attr("data-score", i + 1);
                        break;
                    }
                }
                options.callback();
            });
        });
    }
})(jQuery);