define(function(require, exports, module){
    var template = require("template");
    /**
     * Created by rt.yishuangxi on 2014/12/22.
     */
    /*
     * 次插件要求返回的数据结构为：{success:true,total:1,page:1,records:1,rows:[]}
     * 仅适合该项目，难以通用
     * */
    $.fn.yiLoadMore = function (options) {
        var defaults = {
            templateId: "", //必填：渲染模板
            url: "",//必填：数据源
            first_success: function (data) {//data值为请求成功之后服务器返回的数据。
            },//第一次请求成功回调函数，在首页移除没有经纬度的页面上局，以及请求下来的rows长度为0的时候使用。
            successCount: 0,//记录成功请求次数，主要用于标识是否是第一次请求成功。
            success:function(){}
            /*page:0,//当前页序号
             total:0,//总页数
             loadBtn:null*/
        };

        var options = $.extend({}, defaults, options);
        $.fn.yiLoadMore.options = options;
        var $this = this;

        loadData();

        function init() {
            if (options.page !== options.total && options.total !== 0) {
                if (options.loadBtn) {
                    options.loadBtn.text("点击加载更多");
                } else {
                    options.loadBtn = $('<div class="yi-load-more" style="text-align: center;padding: 10px 0">点击加载更多</div>');
                    //干掉已有的“点击加载更多”
                    if($this.next().hasClass("yi-load-more")){
                        $this.next().remove();
                    }
                    options.loadBtn.insertAfter($this);
                    options.loadBtn.on("click", loadData);
                }
            } else {
                //如果页数已经到达总页数，则解除绑定！并移除加载更多按钮
                if (options.loadBtn) {
                    options.loadBtn.unbind("click", loadData);
                    options.loadBtn.remove();
                }
            }
        }


        function loadData() {
            if (options.loadBtn) {
                options.loadBtn.text("正在加载数据...");
            }
            var ajaxObject = $.ajax({
                url: options.url,
                data: {
                    //加载指定页数
                    pageNo: options.page ? options.page + 1 : 1
                },
                success: function (data) {
                    if (data.success) {
                        if (options.successCount === 0) {
                            options.first_success(data);
                        }
                        options.successCount++;
                        if(data.rows.length === 0){
                            $this.append("");
                            return;
                        }
                        var html = template(options.templateId, data);
                        $this.append(html);
                        options.success();
                        options.page = data.page;
                        options.total = data.total;
                        init();
                    } else {
                        alert("请求服务器失败，请重试！");
                    }
                }
            });
            //这里把ajaxObject作为this的属性返回一下，方便插件外面对该ajax请求做进一步的操作。
            $this.ajaxObject = ajaxObject;
        }

        return this;
    };
});