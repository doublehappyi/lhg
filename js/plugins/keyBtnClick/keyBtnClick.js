/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function(require, exports, module){
    /***
     *
     * @param fn click的回调函数
     *  @param one 是否执行一次性绑定，默认值：true
     * @param resume 改参数只有在one参数为false的时候才能够有效：是否在禁用button之后，又启用该button，默认值：false;
     */
    $.fn.keyBtnClick = function (fn, one, resume) {
        var one = one == false ? false : true,
            resume = resume || false;
        return this.each(function () {
            var _$self = $(this);

            function _clickHandler() {
                var _$this = $(this),
                    ret;
                _$this.addClass("btn-disabled");
                _$this.attr("disabled", "disabled");

                ret = fn.apply(this);
                if (resume && !one) {
                    _$this.removeClass("btn-disabled");
                    this.removeAttribute("disabled");
                }
                return ret;
            }

            if (one) {
                _$self.one("click", _clickHandler);
            } else {
                _$self.bind("click", _clickHandler);
            }
        });
    }
});