/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function (require, exports, module) {
    var template = require("template");

    function MouseWheelMore(selector, options) {
        var defaults = {
            $container: $("body"),//监听滚动条时间的容器
            bottom: 400,//距离底部多远的时候开始加载下一页template: "",//模板id，必传
            url: "",//请求地址，必传
            page: 0,
            total: 1,
            timeout: 300,
            first_success: function (data) {
            },
            is_first_success: true,
            success: function (data) {
            },
            error: function () {
            }

        };
        var options = $.extend({}, defaults, options || {});

        this.options = options;
        this.selector = selector;
        this.$target = $(selector);//里面放加载下来的内容的容器
        $(".add_more").remove();
        this.$target.after('<div class="add_more" style="clear: both;text-align: center;padding: 10px 0;">正在努力加载，请稍后...</div>');
        this.$more = this.$target.next(".add_more");

        this._init();
    };

    MouseWheelMore.prototype = {
        constructor: MouseWheelMore,
        _$body: $("body"),
        allowToScroll: false,//此参数如果为true的话，可以用来屏蔽scroll的监听事件，此参数可以从外部改变
        _init: function () {
            this._resize();
            this._bindEvents();
            this._loadData();
        },
        _bindEvents: function () {
            var self = this;
            //下拉滚动绑定事件
            $(window).scroll(self._scrollHandler.call(self));
            //监听浏览器窗口变化
            window.onresize = this._resize;
        },
        _scrollHandler: function () {
            var self = this;
            return function () {
                if (!self.allowToScroll) {
                    return;
                }
                //记录上一次的scrollTop距离
                self.lastScrollTop = document.documentElement.scrollTop + document.body.scrollTop;
                //记录每一次的滚动
                if (self.timeoutId) {
                    clearTimeout(self.timeoutId);
                    self.timeoutId = setTimeout(self._timeoutHandler.call(self), self.options.timeout);
                } else {
                    self.timeoutId = setTimeout(self._timeoutHandler.call(self), self.options.timeout);
                }
            }
        },
        _timeoutHandler: function () {
            var self = this;
            return function () {
                var bottom = $(document).height() - self.windowHeight - (document.documentElement.scrollTop + document.body.scrollTop);
                if (self.lastScrollTop === (document.documentElement.scrollTop + document.body.scrollTop)) {
                    clearTimeout(self.timeoutId);
                    if (bottom < self.options.bottom
                        && !self.options.loading
                        && self.options.page !== self.options.total) {
                        self.timeoutId = false;
                        self.options.loading = true;//加载开始，把loading值置为true，阻止重复加载
                        self._loadData();
                    }
                }
            }
        },
        _resize: function () {
            this.windowHeight = $(window).height();
        },
        _loadData: function () {
            var self = this;
            self.$more.text("正在努力加载，请稍后...");
            //这里记录ajaxObject主要是方便用户可以终止掉此次的ajax请求，
            //因为如果获取地址成功，就要启用另外一个附带经纬度的ajax请求
            this.ajaxObject = $.ajax({
                url: self.options.url,
                data: {
                    pageNo: self.options.page + 1
                },
                success: function (data) {
                    if (data.success) {
                        if (self.options.is_first_success) {//执行第一次请求成功的回调函数
                            self.$target.empty();//如果是第一次执行成功，把容器$target里面的内容都清除掉
                            self.options.first_success(data);
                            self.options.is_first_success = false;
                        }

                        if (data.page <= data.total && data.total !== 0) {//正常请求下来的数据

                            var html = template(self.options.template, data);
                            self.$target.append(html);
                            self.$more.text("下拉加载更多...");
                            //success函数要放到html的渲染后面
                            self.options.success();

                            self.options.page = data.page;
                            self.options.total = data.total;
                            //如果已经是最后一页
                            if (data.page === data.total) {
                                self.$more.remove();
                            }
                        } else if (data.total === 0) {//加载数据为空
                            self.$more.text("对不起，您还没有任何数据...");
                            self.$more.remove();
                        }
                    } else {
                        self.options.error();
                    }
                },
                complete: function () {
                    self.options.loading = false;//加载完成，把loading值置为false
                    self.allowToScroll = true;//等数据加载完成以后，再允许继续执行
                }
            });
        },
        destory: function () {//如果当前对象不再继续使用了，记得调用此方法！目前在首页index.js的猜你喜欢和商家列表页sjlb.js的商户列表有用到
            var self = this;
            self.ajaxObject.abort();//废除ajax请求
            self.allowToScroll = false;//阻止scroll事件句柄的执行
        }
    };

    module.exports = MouseWheelMore;
});