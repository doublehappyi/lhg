/**
 * Created by YI on 2014/7/16.
 */
define(function(require, exports, module){
    require("plugins/seajs-css/seajs-css");
    //依赖yiLayer.css
    seajs.use("plugins/yiNumber/yiNumber.css");
    $.fn.yiNumber = function (options) {
        var defaults = {
            min: 1,
            max: 100,
            step: 1,
            success: function (json) {},
            error:function(json){},
            remote: false,
            /*data:{},*//* AAA:这里本应该有个参数传入的，但是临时解决一下，使代码严重耦合了,参考BBB*/
            url: "",
            callback:function(){},//非远程请求的回调函数
            num:1
        }
        var options = $.extend({}, defaults, options);

        function ajaxSubmit(url, data, cb) {
            $.ajax(url, {
                async: true,
                cache: false,
                dataType: "JSON",
                data: data,
                method: "POST",
                success: cb
            });
        }

        return this.each(function () {
            var $this = $(this),
                $minus = $this.find(".j-minus"),
                $plus = $this.find(".j-plus"),
                $number = $this.find(".j-number"),
                $input = $this.find("input");

            var inputName = $input.attr("name") || "number";

            main();

            function main() {
                bindEvents();
            }



            function bindEvents() {
                $minus.click(function () {
                    var number = $number.val();
                    //首先判断number是否已经小于最小值了。如果不是最小值，就可以直接忽略用户的点击动作。
                    if (number > options.min) {
                        //再次判断是否是远程请求
                        if (options.remote) {
                            //先记住oldValue
                            var oldValue = $number.val();
                            //先让自减1:这里算是一个组件内部执行的动作。
                            $number.val(number - options.step);
                            remoteRequest(oldValue);
                        } else {
                            $number.val(number - options.step);
                            options.num = parseInt(number) - parseInt(options.step);
                            options.callback();
                        }
                    }
                });

                $plus.click(function () {
                    var number = $number.val();
                    //首先判断number是否已经小于最小值了。如果不是最小值，就可以直接忽略用户的点击动作。
                    if (number < options.max) {
                        //再次判断是否是远程请求
                        //再次判断是否是远程请求
                        if (options.remote) {
                            //先记住oldValue
                            var oldValue = $number.val();
                            //先让自减1:这里算是一个组件内部执行的动作。
                            $number.val(parseInt(number) + parseInt(options.step));
                            remoteRequest(oldValue);
                        } else {

                            $number.val(parseInt(number) + parseInt(options.step));
                            options.num = parseInt(number) + parseInt(options.step);
                            options.callback();
                        }
                    }
                });

                $input.keyup(function (e) {
                    //如果是空，则什么都不做。
                    if ($number.val() == "") {
                        return;
                    }
                    var number = parseInt($number.val());
                    //超过最大值的时候，置为最大值
                    if (number > parseInt(options.max)) {
                        number = options.max;
                    } else if (number < parseInt(options.min)) {
                        number = options.min;
                    }
                    //如果是NaN，则置为1
                    if (isNaN(number)) {
                        number = 1;
                    }

                    $input.val(number);
                    options.num = Number($input.val());
                    options.callback();
                });

                $input.blur(function () {
                    var number = parseInt($number.val());
                    //首先判断number是否已经小于最小值了。如果不是最小值，就可以直接忽略用户的点击动作。
                    if (number < options.max && number > options.min) {
                        //再次判断是否是远程请求
                        if (options.remote) {
                            //先记住oldValue
                            var oldValue = $number.val();
                            remoteRequest(oldValue);
                        }else{
                            //options.num = number;
                            //options.callback();
                        }
                    }else{
                        //$number.val(options.min);
                        //options.num = number;
                        //options.callback();
                    }
                });
            }

            function remoteRequest(oldValue){
                var data = $.extend(options.data,{
                    counts: $input.val(),
                    id:$input.closest("tr").find("[name=id]").attr("data-id")//BBB:本不应该这么来的，这里直接取到id值，参考AAA
                });
                ajaxSubmit(options.url, data, function (json) {
                    if(json.success){
                        //再执行用户的回调函数：这里是提供给用户执行的回调的函数
                        options.success(json,$this);
                    }else{
                        //恢复原来的值
                        $number.val(oldValue);
                        options.error(json,$this);
                    }
                });
            }
        });
    }
});