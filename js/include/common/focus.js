/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function (require, exports, module) {
    var yiLayer = require("plugins/yiLayer/yiLayer");

    exports.focus = function (options) {

        var $add = $(".to-add");
        var $cancel = $(".to-cancel");
        var $cancelVerify = $(".cancel-verify");

        var layer_cancel = new yiLayer("#layer-cancel");
        var layer_add_ok = new yiLayer("#layer-add-ok");
        var layer_error = new yiLayer("#layer-error");
        var layer_cancel_ok = new yiLayer("#layer-cancel-ok");


        //点花灯活动
        var layer_lingqu = new yiLayer("#layer-lingqu");
        var layer_tuijian = new yiLayer("#layer-tuijian");

        var defaults = {
            add_cb: function () {
                //关注成功回调函数
            },
            cancel_cb: function () {
                //取消成功回调函数
                $cancel.hide();
                $add.show();
            }
        };

        var options = $.extend(defaults, options || {});

        $add.click(function () {
            var url = ctxPaths + "/maOrderInfo/addMaFocus.ajax";
            var maStoreId = $add.attr("data-maStoreId");
            $.ajax(url, {
                data: {
                    maStoreId: maStoreId,
                    auto: false
                },
                success: function (data) {
                    if (data.success === true) {
                        $cancel.show();
                        $add.hide();
                        options.add_cb();
                        var activity = data.data.activity;
                        //没有活动
                        if (activity == "0") {
                            layer_add_ok.show();
                        }
                        //送礼活动
                        else if (activity == "1") {
                            layer_lingqu.show();
                            layer_lingqu.$layer.find(".j-cash").text(data.data.cash + "元");
                            var $lingqu = layer_lingqu.$layer.find(".j-lingqu");
                            var cashUrl = data.data.cashUrl;
                            var memberId = data.data.memberId;

                            //领取动作
                            $lingqu.click(function () {
                                var url = ctxPaths + "/maOrderInfo/receiveCash.ajax";
                                //ajax请求为后台计数
                                $.ajax({
                                    url: url,
                                    data: {
                                        memberId: memberId
                                    },
                                    complete: function () {
                                        location = cashUrl;//领取的页面跳转
                                    }
                                });
                            });
                        }
                        //已经送过了
                        else if (activity == "2") {
                            layer_tuijian.show();
                            var $tuijian = layer_tuijian.$layer.find(".btn_1");
                            $tuijian.attr("href", data.data.cashUrl);
							layer_tuijian.$layer.find(".btn_2").unbind('click').bind('click',function(){
								layer_tuijian.hide();
							});

                        } else {
                            alert("activity error:" + activity);
                        }

                    } else if (data.success === false) {
                        layer_error.show();
                    }
                }
            });
        });


        $cancel.click(function () {
            /*var $cancelBtn = $(this);//被点击的取消按钮，主要用在我的关注列表页面指这里给$cancelBtn赋值*/
            layer_cancel.show();
        });
        $cancelVerify.click(function () {
            layer_cancel.hide();
            var url = ctxPaths + "/maOrderInfo/cancelFocus.ajax";
            var maStoreId = $cancel.attr("data-maStoreId");
            $.ajax(url, {
                data: {
                    maStoreId: maStoreId,
                    auto: false
                },
                success: function (data) {
                    if (data.success === true) {
                        //确认用户已经登陆
                        layer_cancel_ok.show();
                        options.cancel_cb($cancel);
                    } else if (data.success === false) {
                        layer_error.show();
                    }
                }
            });
        });
    }
});