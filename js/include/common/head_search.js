/**
 * Created by rt.yishuangxi on 2015/1/21.
 */
define(function(require, exports, module){
    //搜索框功能
    search();
    //城市定位
    $("#city-name").text(getCookie("cityName") || "济南");

    /*
     * 依赖2个特定的css类：search-input,search-btn
     * */
    function search() {
        $(".search-btn").click(function () {
            var soKey = $(".search-input").val();
            if (soKey === "" || soKey === "输入商家、品类、商圈" || soKey === "请输入商户名称、地址等") {
                alert("搜索关键字不能为空!");
            } else {
                location = ctxPaths + "/pages/shop/search.shtml?soKey=" + encodeURIComponent(soKey);
            }
        });

        //清空搜索框
        $('.search').find(':text').focus(function(){
            if(!this.initValue){
                this.initValue = this.value;
            }
            if(this.value === this.initValue){
                this.value = '';
            }
        }).blur(function(){
            if(this.value === '' || this.value === null){
                this.value = this.initValue;
            }
        });
    }

    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=")
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1
                c_end = document.cookie.indexOf(";", c_start)
                if (c_end == -1) c_end = document.cookie.length
                return unescape(document.cookie.substring(c_start, c_end))
            }
        }
        return ""
    }
});