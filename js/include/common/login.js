/**
 * Created by rt.yishuangxi on 2015/1/21.
 */
define(function (require, exports, module) {
    var yiLayer = require("plugins/yiLayer/yiLayer");

    $(document).ajaxComplete(function (event, xhr, settings) {
        try {
            var result = $.parseJSON(xhr.responseText);
            // 用户会话失效或未登录，重定向至登录页面
            //如果有auto参数为false，则先弹出登录提示窗口
            if (result && result['isLogin'] == "false" && result['url']) {
                if (result['auto'] == "false") {
                    var layer_login = new yiLayer("#layer-login");
                    layer_login.show();
                    $(".login-now").attr("href", result['url']);
                } else {
                    window.location.replace(result['url']);
                }
            }
        } catch (e) {
        }
    });
});