/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function (require, exports, module) {
    var yiLayer = require("plugins/yiLayer/yiLayer");
    exports.pay_tips = function () {
        //全部订单里面的订单支付功能
        $(".pay_order").click(function (e) {
            e.stopPropagation();
            var $this = $(this),
                form = this.form;
            var PAY_INFO_URL = ctxPaths + "/pay/getPayInfo.ajax";
            //submit按钮点击之后是否执行form提交动作的标记
            var ret;

            var maOrderInfoId = $this.attr("data-maOrderInfoId");
            $.ajax(PAY_INFO_URL, {
                type: "GET",
                async: false,
                data: {
                    noPayOrderId: maOrderInfoId
                },
                success: function (json) {
                    if (json.success) {
                        //获取订单id：字段FlowCode
                        ret = true;
                        if (!json.data.payUrl) {
                            return;
                        }
                        //处理form元素
                        form.action = json.data.payUrl;
                        for (var p in json.data) {
                            var newElement = document.createElement("input");
                            newElement.setAttribute("name", p);
                            newElement.setAttribute("type", "hidden");
                            newElement.setAttribute("value", json.data[p]);
                            form.appendChild(newElement);
                        }

                        var maOrderInfoId = json.data.FlowCode;

                        var href = ctxPaths + "/pages/shop/pay_result.shtml?maOrderInfoId=" + maOrderInfoId;
                        $("#pay_complete").attr("href", href);
                        $("#pay_error").attr("href", href);

                        var pay_tips = new yiLayer("#pay_tips");
                        pay_tips.show();

                    } else {
                        if (json.code === "bindMobile") {
                            location = json.message;
                        } else {
                            alert(json.message);
                        }
                        ret = false;
                    }
                },
                error: function () {
                    ret = false;
                }
            });

            return ret;
        });
    };
});