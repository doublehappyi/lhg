/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function (require, exports, module) {
    exports.fama = function () {
        $(".b_1").click(function (e) {
            e.stopPropagation();
            var $me = $(this);
            var maOrderInfoId = $(this).attr("data-maOrderInfoId"),
                maOrderGoodsId = $(this).attr("data-maOrderGoodsId");
            var FAMA_URL = ctxPaths + "/maOrderInfo/sendCode.ajax?maOrderInfoId="
                + maOrderInfoId + "&maOrderGoodsId=" + maOrderGoodsId;
            $.ajax(FAMA_URL, {
                type: "GET",
                success: function (data) {
                    if (data.success) {
                        $me.next(".b_1_text").text("已发送");
                    } else {
                        $me.next(".b_1_text").text("发送失败");
                    }
                },
                error: function () {
                    alert("系统错误，请重试！");
                }
            });
        });
    }

    exports.fama_disabled = function () {
        $(".btn-disabled").click(function (e) {
            e.stopPropagation();
            return false;
        });
    }
});