/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function (require, exports, module) {
    var yiLayer = require("plugins/yiLayer/yiLayer");

    $(function () {
        /*返回按钮全局绑定*/
        $(".history-back").click(function () {
            history.back();
        });

        /*临时解决退出登录，两边平台无法同步退出的问题*/
        var $logout = $(".jt-navBoxUser-login");
        if ($logout.length > 0) {
            if ($logout.text() == "退出") {
                $logout.click(function () {
                    var url = ctxPaths + "/portalWeb/ssoLogout.ajax";
                    $.ajax(url, {
                        async: false,
                        success: function () {
                            utils.log("logout success");
                        },
                        error: function () {
                            utils.log("logout error");
                        }
                    });
                });
            }
        }
    });
});