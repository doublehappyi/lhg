/**
 * Created by rt.yishuangxi on 2015/1/10.
 */
define(function (require, exports, module) {
    /***
     * 函数功能:获取location.href中的某个指定字段的值，如果没有这个字段，则返回空字符串
     * @param field ：location.href中包含字段
     * @param href ： 可选参数，没有传入则默认使用location.href获取
     * @returns {string} ：返回值类型为字符串
     */
    exports.getHrefField = function (field, href) {
        var ret = "";
        var href = href || location.href;
        var fieldStr, fieldArr, index;
        if (href.indexOf("?") !== -1) {
            fieldStr = href.substring(href.indexOf("?") + 1);
            fieldArr = fieldStr.split("&");
            for (var i = 0, len = fieldArr.length; i < len; i++) {
                index = fieldArr[i].indexOf(field + "=");
                if (index !== -1) {
                    ret = fieldArr[i].substring(index + (field + "=").length);
                    break;
                }
            }

            return decodeURIComponent(ret);
        } else {
            return decodeURIComponent(ret);
        }
    };


    /****
     * 获取元素的占位高度
     * @param $dom 一定要是jquery对象
     * @returns {*}
     */
    exports.getRealHeight = function ($dom) {
        var h = $dom.height() +
            parseInt($dom.css("padding-top")) +
            parseInt($dom.css("padding-bottom")) +
            parseInt($dom.css("margin-top")) +
            parseInt($dom.css("margin-bottom")) +
            parseInt($dom.css("border-top-width")) +
            parseInt($dom.css("border-bottom-width"));
        return h;
    };

    /***
     * 获取元素的占位宽度
     * @param $dom 一定要是jquery对象
     * @returns {*}
     */
    exports.getRealWidth = function ($dom) {
        var w = $dom.width() +
            parseInt($dom.css("padding-left")) +
            parseInt($dom.css("padding-right")) +
            parseInt($dom.css("margin-left")) +
            parseInt($dom.css("margin-right")) +
            parseInt($dom.css("border-left-width")) +
            parseInt($dom.css("border-right-width"));
        return w;
    };


    /***
     * 获取定位函数
     * @param success：获取位置成功的回调函数
     * @param error:获取位置失败的回调函数
     */
    exports.getPosition = function (success, failure) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(getPositionSuccess, getPositionError, {
                enableHighAccuracy: true,
                maximunAge: 60000,//缓存时间配置：1分钟之内，再次获取位置时候，使用这次获取到的地址
                timeout: 10000
            });
        } else {
            //alert("对不起，您的浏览器不支持地理定位功能，请使用chrome或者firefox浏览器浏览！");
            return;
        }

        function getPositionSuccess(position) {
            success(position);
        }

        function getPositionError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    //alert('位置请求被拒绝!');
                    break;
                case error.POSITION_UNAVAILABLE:
                    //alert('位置获取失败!');
                    break;
                case error.TIMEOUT:

                    //alert('位置获取超时！');
                    break;
            }

            if (failure) {
                failure(error);
            }
        }
    };

    exports.addLoading = function (selector, text) {
        var text = text ? text : "正在努力加载数据，请稍后...";
        var content = "<div class='yi-loading' style='text-align: center'>" + text + "</div>";
        $(content).prependTo(selector);
    };

    exports.clearLoading = function (selector) {
        $(selector).find(".yi-loading").remove();
    };

    /***
     * 打印调试信息的工具函数，以免兼容性问题出现
     */
    exports.log = function () {
        if (window.console && window.console.log) {
            console.log(arguments);
        }
    }
});