/**
 * Created by rt.yishuangxi on 2014/12/19.
 */
define(function(require, exports, module){
    var utils = require("utils");
    //require("plugins/yiLoadMore/yiLoadMore");
    var pay_fama = require("include/order/pay_fama");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");
    $(function () {
        yifukuan();//已付款
    });

    function yifukuan() {
        var url = ctxPaths + "/maOrderInfo/queryMyOrder.ajax?orderCodeStatus=" + utils.getHrefField("orderCodeStatus");
        /*$("#order_yfk_container").yiLoadMore({
            url: url,
            templateId: "order_yfk",
            success:function(){
                pay_fama.fama();//定义在/include/order/pay_fama.html里面的全局函数
                pay_fama.fama_disabled();//定义在/include/order/pay_fama.html里面的全局函数
            }
        });*/

        var m = new MouseWheelMore("#order_yfk_container",{
            url: url,
            template: "order_yfk",
            success: function () {
                pay_fama.fama();//定义在/include/order/pay_fama.html里面的全局函数
                pay_fama.fama_disabled();//定义在/include/order/pay_fama.html里面的全局函数
            }
        });
    }
});