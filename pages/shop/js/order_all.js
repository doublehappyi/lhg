/**
 * Created by rt.yishuangxi on 2014/12/19.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    //require("plugins/yiLoadMore/yiLoadMore");
    var pay_fama = require("include/order/pay_fama");
    var pay_tips = require("include/order/pay_tips");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");
    $(function () {
        order_all();//已付款
    });

    function order_all() {
        var url = ctxPaths + "/maOrderInfo/queryMyOrder.ajax?orderCodeStatus=" + utils.getHrefField("orderCodeStatus");
        /*$("#order_all_container").yiLoadMore({
            url: url,
            templateId: "order_all",
            success: function () {
                pay_tips.pay_tips();//全局函数，通过include/order/pay_tips.html引入的
                pay_fama.fama();//定义在/include/order/pay_fama.html里面的全局函数
                pay_fama.fama_disabled();//定义在/include/order/pay_fama.html里面的全局函数
            }
        });*/

        var m = new MouseWheelMore("#order_all_container",{
            url: url,
            template: "order_all",
            success: function () {
                pay_tips.pay_tips();//全局函数，通过include/order/pay_tips.html引入的
                pay_fama.fama();//定义在/include/order/pay_fama.html里面的全局函数
                pay_fama.fama_disabled();//定义在/include/order/pay_fama.html里面的全局函数
            }
        });
    }
});