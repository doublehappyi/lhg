/**
 * Created by rt.yishuangxi on 2014/12/22.
 */
define(function (require, exports, module) {
    var template = require("template");
    var utils = require("utils");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");

    $(function () {
        fenlei();//全部分类
        district();//全城
        fujinHandler();//给附近的点击绑定事件，因为附近是静态的html代码，不需要放在ajax请求后面，而districtHandler和flHandler则是！

        //如果有查询条件，则使用查询地址，如果没有，则使用推荐地址
        if (utils.getHrefField("distance") || utils.getHrefField("storeTypeId") || utils.getHrefField("district")) {
            //如果有距离查询,是一定要获取经纬度的
            if (utils.getHrefField("distance")) {
                shangjia_query_with_distance();
            }
            //如果没有距离查询，可以先不获取经纬度
            else {
                shangjia_query_no_distance();
            }
        } else {
            //加载默认的推荐商户：先不获取经纬度，然后再尝试获取经纬度
            var url = ctxPaths + "/wapStoreController/mainPageQuery.ajax";
            var lastM = shangjia(url);
            shangjia_with_position(url, lastM);
        }
    });

    function fenlei() {
        var url = ctxPaths + "/codedictCon/getStoreCategory.ajax";
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    var html = template("fenlei", data);
                    $(html).insertAfter("#products");
                    flHandler();
                } else {

                }
            }
        });
    }

    function flHandler() {
        var $fenlei = $(".j-fenlei");
        var storeTypeId = utils.getHrefField("storeTypeId");
        var attr = "data-storeTypeId";
        if (storeTypeId) {
            $(".j-fenlei-curr").attr(attr, storeTypeId).text(getText($fenlei, attr, storeTypeId));
        }

        $fenlei.click(function () {
            var $district = $(".j-district-curr");
            var district = $district.attr("data-district") ? "district=" + $district.attr("data-district") + "&" : "";
            var storeTypeId = $(this).attr(attr) ? "storeTypeId=" + $(this).attr(attr) + "&" : "";
            var distance = $district.attr("data-distance") ? "distance=" + $district.attr("data-distance") + "&" : "";

            //如果有距离，则需要使用获取位置函数
            if (distance) {
                location = ctxPaths + "/pages/shop/sjlb.shtml?" + storeTypeId + district + distance;
                /*utils.getPosition(function (position) {
                 var latitude = position.coords.latitude,
                 longitude = position.coords.longitude;
                 var p = "latitude=" + latitude + "&longitude=" + longitude;
                 location = ctxPaths + "/pages/shop/sjlb.shtml?" + storeTypeId + district + distance + p;
                 });*/
            } else {
                location = ctxPaths + "/pages/shop/sjlb.shtml?" + storeTypeId + district;
            }

        });
    }

    function districtHandler() {
        var $district = $(".j-district");
        var disrict = utils.getHrefField("district");
        var attr = "data-district";
        if (disrict) {
            $(".j-district-curr").attr(attr, utils.getHrefField("district")).text(getText($district, attr, disrict));
        }

        $(".j-district").click(function () {
            var storeTypeId = $(".j-fenlei-curr").attr("data-storeTypeId") ? "storeTypeId=" + $(".j-fenlei-curr").attr("data-storeTypeId") + "&" : "";
            var district = $(this).attr(attr) ? "district=" + $(this).attr(attr) + "&" : "";
            location = ctxPaths + "/pages/shop/sjlb.shtml?" + district + storeTypeId;
        });


    }

    function fujinHandler() {
        var $fujin = $(".j-fujin");
        var distance = utils.getHrefField("distance");
        var attr = "data-distance";
        if (distance) {
            $(".j-district-curr").attr(attr, distance).text(getText($fujin, attr, distance));
        }

        $(".j-fujin").click(function () {
            var storeTypeId = $(".j-fenlei-curr").attr("data-storeTypeId") ? "storeTypeId=" + $(".j-fenlei-curr").attr("data-storeTypeId") + "&" : "";
            var distance = $(this).attr(attr) ? "distance=" + $(this).attr(attr) + "&" : "";
            location = ctxPaths + "/pages/shop/sjlb.shtml?" + storeTypeId + distance;
            /*utils.getPosition(function (position) {
             var storeTypeId = $(".j-fenlei-curr").attr("data-storeTypeId") ? "storeTypeId=" + $(".j-fenlei-curr").attr("data-storeTypeId") + "&" : "";
             var distance = $(this).attr(attr) ? "distance=" + $(this).attr(attr) + "&" : "";
             var latitude = position.coords.latitude,
             longitude = position.coords.longitude;
             var p = "latitude=" + latitude + "&longitude=" + longitude;
             location = ctxPaths + "/pages/shop/sjlb.shtml?" + storeTypeId + distance + p;
             });*/
        });
    }

    function getText($dom, attr, value) {
        var text = "";
        for (var i = 0; i < $dom.length; i++) {
            if ($($dom[i]).attr(attr) == value) {
                text = $($dom[i]).text();
                break;
            }
        }

        return text;
    }

    function district() {
        var url = ctxPaths + "/wapStoreController/getRegionListByParentCode.ajax";
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    var html = template("district", data);
                    $(html).appendTo("#district-container");
                    districtHandler();
                } else {

                }
            }
        });
    }

    /*function shangjia_query() {
     var url = ctxPaths + "/wapStoreController/queryByKey.ajax?" +
     "district=" + utils.getHrefField("district") +
     "&storeTypeId=" + utils.getHrefField("storeTypeId") +
     "&distance=" + utils.getHrefField("distance");
     var $pdbox = $(".pd_box").yiLoadMore({
     url: url,
     templateId: "shangjia",
     first_success: function (data) {
     noDataTips(data);
     }
     });

     return $pdbox.ajaxObject;
     }*/

    function shangjia_query_no_distance() {
        var url = ctxPaths + "/wapStoreController/queryByKey.ajax?" +
            "district=" + utils.getHrefField("district") +
            "&storeTypeId=" + utils.getHrefField("storeTypeId");
        var lastM = shangjia(url);
        shangjia_with_position(url, lastM);
    }

    function shangjia_query_with_distance() {
        utils.addLoading(".pd_box", "正在获取您的位置,请稍后...");
        utils.getPosition(function (position) {
            utils.clearLoading(".pd_box");
            var url = ctxPaths + "/wapStoreController/queryByKey.ajax?" +
                "storeTypeId=" + utils.getHrefField("storeTypeId") +
                "&distance=" + utils.getHrefField("distance") +
                "&storeMapLat=" + position.coords.latitude + "&storeMapLong=" + position.coords.longitude;

            shangjia(url);
        }, function () {
            utils.clearLoading(".pd_box");
            tuijian();
        });
    }

    /***
     *
     * @param url 商户请求地址
     * @param if_position 是否启用经纬度查询？
     */
    function shangjia(url) {
        /*var $pdbox = $(".pd_box").yiLoadMore({
         url: url,
         templateId: "shangjia",
         first_success: function (data) {
         utils.clearLoading(".pd_box");
         $pdbox.empty();
         noDataTips(data);
         }
         });
         return $pdbox.ajaxObject;*/

        var m = new MouseWheelMore(".pd_box", {
            url: url,
            template: "shangjia",
            first_success: function (data) {
                noDataTips(data);
            }
        });
        return m;
    }


    function shangjia_with_position(url, lastM) {
        utils.getPosition(function (position) {
            var latitude = position.coords.latitude,
                longitude = position.coords.longitude;

            //因为这个url可能来自2个地方：
            //如果url里面已经有?号了，那代表url已经有查询条件了，所以这里接地址查询条件用&号，否则用?
            if (url.indexOf("?") > 0) {url = url + "&" + "storeMapLat=" + latitude + "&storeMapLong=" + longitude;
            } else {
                url = url + "?" + "storeMapLat=" + latitude + "&storeMapLong=" + longitude;
            }

            /*var $pdbox = $(".pd_box");
             $pdbox.yiLoadMore({
             url: url,
             templateId: "shangjia",
             first_success: function (data) {
             ajaxObject.abort();
             $pdbox.empty();
             noDataTips(data);
             }
             });*/

            var m = new MouseWheelMore(".pd_box", {
                url: url,
                template: "shangjia",
                first_success: function (data) {
                    lastM.destory();
                    $(".pd_box").empty();
                    noDataTips(data);
                }
            });
        });
    }

    //如果请求之后，发现没有数据，就显示没有数据的提示
    function noDataTips(data) {
        if (data.rows.length === 0) {
            tuijian();
        }
    }

    //推荐商户
    function tuijian() {
        utils.addLoading(".pd_box");
        var url = ctxPaths + "/wapStoreController/promotionPageQuery.ajax?pmtType=2";
        $.ajax({
            url: url,
            success: function (data) {
                utils.clearLoading(".pd_box");
                if (data.success) {
                    $(".pd_box").empty();
                    var html = template("tuijian", data);
                    $(html).appendTo(".pd_box");
                    $(".tj_shop").show();
                    $(".search_tj_none").show();
                    districtHandler();
                }
            }
        });
    }
});
