/**
 * Created by rt.yishuangxi on 2014/12/19.
 */
define(function(require, exports, module){
    //require("plugins/yiLoadMore/yiLoadMore");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");
    $(function () {
        guanzhu();
    });

    function guanzhu() {
        var url = ctxPaths + "/maOrderInfo/queryMyFocus.ajax";
        /*$(".sm_list_main").yiLoadMore({
            url: url,
            templateId: "guanzhu",
            first_success: function (data) {
                if (data.rows.length === 0) {
                    $(".sm_list_main").empty().html('<div style="text-align: center;color: #f00;height: 100px;margin-top:150px;">我的关注空空如也！</div>')
                }
            },
            success: function () {
                quxiao_guanzhu();
            }
        });*/

        var m = new MouseWheelMore(".sm_list_main",{
            url: url,
            template: "guanzhu",
            first_success: function (data) {
                if (data.rows.length === 0) {
                    $(".sm_list_main").empty().html('<div style="text-align: center;color: #f00;height: 100px;margin-top:150px;">我的关注空空如也！</div>')
                }
            },
            success: function () {
                quxiao_guanzhu();
            }
        });
    }

    function quxiao_guanzhu(){
        //取消关注
        $(".b_2").click(function () {
            var maStoreId = $(this).attr("data-maStoreId");
            var $this = $(this);
            var QUXIAOGUANZHU_URL = ctxPaths + "/maOrderInfo/cancelFocus.ajax";
            $.ajax(QUXIAOGUANZHU_URL, {
                data: {
                    maStoreId: maStoreId
                },
                success: function (data) {
                    if (data.success) {
                        alert("取消关注成功！");
                        $this.closest(".my_gz").remove();
                    } else {
                        alert("取消失败，请重试！");
                    }
                },
                error: function () {
                    alert("取消失败！系统或网络问题，请重试！");
                }
            });
        });
    }
});

