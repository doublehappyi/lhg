/**
 * Created by ouyangfeng on 2014/9/4.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    var template = require("template");
    var focus = require("include/common/focus");
    $(function () {
        var maGoodsId = utils.getHrefField("maGoodsId"),
            maStoreId = utils.getHrefField("maStoreId"),
            success = utils.getHrefField("success"),
            href = ctxPaths + "/pages/shop/check_result.shtml?maStoreId=" + maStoreId + "&maGoodsId=" + maGoodsId;
        var SHANGHUXIANGQING_URL = ctxPaths + "/wapStoreController/getStoreDetail.ajax?maStoreId=" + maStoreId;
        var GET_CHECK_RESULT = ctxPaths + "/pay/get0yTicket.ajax?maGoodsId=" + maGoodsId;

        var $checkResult = $("#check_result");
        if (success === "true") {
            $.get(SHANGHUXIANGQING_URL, function (json) {
                if (json.success) {
                    var html = template("right-tpl", json.data);
                    $checkResult.html(html);
                    focus.focus();

                    $(".history-back").click(function () {
                        history.back();
                    });

                }
            });
        } else {
            var html = template("error-tpl");
            $checkResult.html(html);

            var i = 15;
            var $reget = $("#re-get"),
                message = decodeURI(utils.getHrefField("message"));

            //success为false是前提！
            //为空的情况下，说明已经获取不到了，提示错误信息！
            //不为空的情况下，就是网络异常！
            if (message === "") {
                var intervalId = setInterval(function () {
                    i--;
                    $reget.text("重新获取(" + i + "S)");
                    if (i === 0) {
                        clearInterval(intervalId);
                        $reget.text("重新获取").removeClass("b_3").addClass("b_2").click(function () {

                            $.ajax(GET_CHECK_RESULT, {
                                success: function (data) {
                                    if (data.success) {
                                        location = href + "&success=true";

                                    } else {
                                        location = href + "&success=false&message=" + data.message;
                                    }
                                },
                                error: function () {
                                    location = href + "&success=false&message=''";
                                }
                            });

                        });
                    }
                }, 1000);
            } else {
                $reget.css("display", "none");
                $("#message-error").text(message);
            }

            $(".history-back").click(function () {
                history.back();
            });
        }
    });
});