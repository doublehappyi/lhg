/**
 * Created by yishuangxi qq:2546618112 on 2014/8/31.
 */
define(function(require, exports, module){
    var utils = require("utils");
    require("plugins/yiNumber/yiNumber");

    $(function () {
        var maGoodsId = utils.getHrefField("maGoodsId"),
            maStoreId = utils.getHrefField("maStoreId"),
            orderingNum = utils.getHrefField('orderingNum');
        var ORDER_DETAIL_URL = ctxPaths + "/wapGoodsController/orderingPreview.ajax?maStoreId=&" + maStoreId
            + "&maGoodsId=" + maGoodsId + "&orderingNum=" + orderingNum;

        $.get(ORDER_DETAIL_URL, function (json) {
            if (json.success) {
                var data = json.data;
                $("#goods-name").text(data.goodsName);
                $("#goods-price").text("￥" + data.goodsPrice2 + " 元");
                $("#goods-count").text(data.orderingNum);
                $("#goods-total").text("￥" + data.totalFee + " 元");
                $("#goods-discount").text("￥" + data.memberDiscount + " 元");
                $("#goods-pay").text("￥" + data.totalFee + " 元");

                bindEvents(data);
            } else {
                alert(data.message || "获取商品详情失败，请刷新页面重试");
            }
        });
    });

    function bindEvents(json) {
        var data = json;
        //初始化数量插件
        $(".yiNumber").yiNumber({
            max: 100,
            callback:function(){
                var num = this.num;
                $("#goods-pay").text("￥" + data.goodsPrice2*Number(num).toFixed(2) + " 元");
                $("#goods-total").text("￥" + data.goodsPrice2*Number(num).toFixed(2) + " 元");
            }
        });

        $("#order-next").keyBtnClick(function () {
            var PAY_INFO_URL = ctxPaths + "/pay/getPayInfo.ajax";
            //submit按钮点击之后是否执行form提交动作的标记
            var ret;
            $.ajax(PAY_INFO_URL, {
                async: false,
                data: {
                    maGoodsId: data.maGoodsId,
                    num: data.orderingNum,
                    auto: false
                },

                success: function (json) {
                    utils.log("json : ", json);
                    if (json.success === true) {
                        //获取订单id：字段FlowCode
                        ret = true;
                        if (!json.data.payUrl) {
                            return;
                        }

                        //处理form元素
                        var form = document.getElementById("pay-form");
                        form.action = json.data.payUrl;
                        for (var p in json.data) {
                            var newElement = document.createElement("input");
                            newElement.setAttribute("name", p);
                            newElement.setAttribute("type", "hidden");
                            newElement.setAttribute("value", json.data[p]);
                            form.appendChild(newElement);
                        }

                        var maOrderInfoId = json.data.FlowCode;

                        var href = ctxPaths + "/pages/shop/pay_result.shtml?maOrderInfoId=" + maOrderInfoId;
                        $("#pay_complete").attr("href", href);
                        $("#pay_error").attr("href", href);

                        /*$("#content_5").show();
                         $("#background").css("display", "block");
                         conPosition();*/
                        var pay_tips = new yiLayer("#pay_tips");
                        pay_tips.show();

                    } else if (data.success === false) {
                        if (json.code === "bindMobile") {
                            alert("未绑定手机号！");
                            location = json.message;
                        } else {
                            alert(json.message);
                        }
                        ret = false;
                    } else {
                        ret = false;
                    }
                },
                error: function () {
                    ret = false;
                }
            });
            return ret;
        }, false, true);

        $(".back").click(function () {
            history.back();
        });
    }
});