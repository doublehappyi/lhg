/**
 * Created by rt.yishuangxi on 2014/12/28.
 */
define(function (require, exports, module) {
    $(function () {
        yonghu();//用户信息
        dingdan();//我的订单
        guanzhu();//关注商户
        xiaoxi();//消息盒子
        jiaru();//申请加入
    });


    function yonghu() {
        var url = ctxPaths + "/maOrderInfo/getUserLogin.ajax";
        $.ajax({
            url: url,
            data: {
                auto: false
            },
            success: function (data) {
                if (data.success) {
                    $("#mobNum").text(data.rows[0]["mobNum"]);
                    $("#maUserId").text(data.rows[0]["maUserId"]);
                    $(".login").show();
                } else {
                    $(".un_login").show();
                    $(".j-login-now").attr("href", data['url']);
                }
            }
        });
    }

    function dingdan() {
        var url = ctxPaths + "/maOrderInfo/queryMyOrder.ajax?orderCodeStatus=9";
        $.ajax({
            url: url,
            data: {
                auto: false
            },
            success: function (data) {
                if (data.success) {
                    $("#dingdan").text(data.records);
                }
            }
        });
    }

    function guanzhu() {
        var url = ctxPaths + "/maOrderInfo/queryMyFocus.ajax";
        $.ajax({
            url: url,
            data: {
                auto: false
            },
            success: function (data) {
                if (data.success) {
                    $("#guanzhu").text(data.records);
                }
            }
        });
    }

    function xiaoxi() {
        var url = ctxPaths + "/maOrderInfo/queryPromotionMsg.ajax";
        $.ajax({
            url: url,
            data: {
                auto: false
            },
            success: function (data) {
                if (data.success) {
                    $("#xiaoxi").text(data.records);
                }
            }
        });
    }

    function jiaru() {
        var url = ctxPaths + '/maBusinessApply/queryIsLogin.ajax';
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    $(".admin-m").attr("href",data.data.storePortalUrl);
                    var isRegister = data.data.isRegister;
                    //已注册1
                    if(isRegister == "1"){
                        $(".add_sj").hide();
                    }
                    //未注册isRegister == 0
                    else{
                        $(".add_sj").show();
                    }
                }
            }
        });
    }
});
