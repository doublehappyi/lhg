/**
 * Created by rt.yishuangxi on 2014/12/25.
 */
define(function (require, exports, module) {
    var template = require("template");
    var utils = require("utils");
    require("plugins/yiLoadMore/yiLoadMore");
    var yiLayer = require("plugins/yiLayer/yiLayer");
    var focus = require("include/common/focus");

    $(function () {
        sjxq();//商家详情
        splb();//商品列表
    });

    function sjxq() {
        var maStoreId = utils.getHrefField("maStoreId");
        var url = ctxPaths + "/wapStoreController/getStoreDetail.ajax?maStoreId=" + maStoreId;
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    var html = template("sjxq", data.data);
                    $(html).appendTo("#sjxq-container");

                    if(data.data.nearByBus){
                        $("#gjlx").text(data.data.nearByBus);//公交路线
                    }else{
                        $("#gjlx").text("暂无");//公交路线
                    }
                    $("#sjjj").html(data.data.storeDesc);//商家简介
                    //$("#gdfd").attr("href", "pages/shop/list_more.shtml?maStoreId=" + maStoreId);//更多分店

                    //要等到页面渲染成功的时候，才可以执行这个函数
                    focus.focus();


                    //如果有多个号码由空格隔开，则使用弹窗功能
                    var shopTel = data.data.shopTel;
                    var shopTelArr = shopTel.split(" ");
                    if (shopTelArr.length > 1) {
                        var $phone = $(".phone");
                        var $a = $phone.find("a");
                        $a.attr("href", "javascript:;");
                        var layerHtml = template("phone-layer-template", {rows: shopTelArr});
                        $("body").append(layerHtml);
                        var phoneLayer = new yiLayer("#phone-layer");
                        $phone.click(function () {
                            phoneLayer.show();
                        });
                    }
                }
            }
        });
    }


    function splb() {
        var maStoreId = utils.getHrefField("maStoreId");
        var url = ctxPaths + "/wapGoodsController/getGoodsList.ajax?maStoreId=" + maStoreId;
        /* $.ajax({
         url: url,
         success: function (data) {
         if (data.success) {
         var html = template("splb", data);
         $(html).appendTo(".list_que");
         }
         }
         });*/

        $(".list_que").yiLoadMore({
            url: url,
            templateId: "splb"
        });
    }

});

