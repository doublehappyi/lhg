/**
 * Created by rt.yishuangxi on 2015/1/29.
 */
define(function (require, exports, module) {
    var template = require("template");
    $(function () {
        fenlei();//商户分类
        city();//选择城市
        $(".j-sjsq").click(function () {
            var data = {
                legalPersonName: $(".legalPersonName").val(),
                legalPersonMobile: $(".legalPersonMobile").val(),
                email: $(".email").val(),
                regionCode: $(".regionCode").val(),
                areaCode: $(".areaCode").val(),
                addressDetail: $(".addressDetail").val(),
                businessName: $(".businessName").val(),
                storeTypeId: $(".storeTypeId").val(),
                storeCateId: $(".storeCateId").val(),
                cooperationContent: $(".cooperationContent").val()
            };
            var $formValidator = $("#form-validator");
            $formValidator.empty();

            var nameReg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）;—|{}【】‘；：”“'。，、？]");
            var phoneReg = /^0?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
            var emailReg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;

            var flag = 0;

            if (nameReg.test(data.legalPersonName) || data.legalPersonName.length > 20 || data.legalPersonName.length === 0) {
                $('<div class="yz_tips">' + '姓名不合法' + '</div>').appendTo($formValidator);
                flag++;
            }
            if (!phoneReg.test(data.legalPersonMobile) || data.legalPersonMobile.length !== 11 || data.legalPersonMobile.length === 0) {
                $('<div class="yz_tips">' + '手机号码不合法' + '</div>').appendTo($formValidator);
                flag++;
            }


            if (!emailReg.test(data.email) || data.email.length > 30 || data.email.length === 0) {
                $('<div class="yz_tips">' + '邮箱不合法' + '</div>').appendTo($formValidator);
                flag++;
            }

            if (data.addressDetail.length > 25 || data.addressDetail.length === 0) {
                $('<div class="yz_tips">' + '地址不合法' + '</div>').appendTo($formValidator);
                flag++;
            }


            if (data.businessName.length > 64 || data.businessName.length === 0) {
                $('<div class="yz_tips">' + '商户名称不合法' + '</div>').appendTo($formValidator);
                flag++;
            }

            if (data.cooperationContent.length > 2000 || data.cooperationContent.length === 0) {
                $('<div class="yz_tips">' + '合作内容不能超过2000个字,不能为空' + '</div>').appendTo($formValidator);
                flag++;
            }

            if(flag > 0){
                return false;
            }

            var url = ctxPaths + '/maBusinessApply/addApply.ajax';
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (data) {
                    if (data.success) {
                        location = ctxPaths + "/pages/shop/sjsq_result.shtml";
                    } else {
                        alert(data.msg || "申请失败");
                    }
                }
            });
        });
    });

    function fenlei() {
        var url = ctxPaths + "/codedictCon/getStoreCategory.ajax";
        var $storeCateId = $(".storeCateId");
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    var $storeTypeId = $(".storeTypeId");
                    var html = template("fenlei", data);
                    $storeTypeId.append(html);

                    //马上渲染二级
                    var html2 = template("fenlei", {data: data.data[0]["subCodeDict"]});
                    $storeCateId.html(html2);

                    //一级的切换绑定事件
                    $storeTypeId.change(function () {
                        var i = this.options.selectedIndex;
                        var html2 = template("fenlei", {data: data.data[i]["subCodeDict"]});
                        $storeCateId.html(html2);
                    });

                }
            }
        });
    }

    function city() {
        var city_url = ctxPaths + "/maDataDict/getRegionListByParentCode.ajax?parentCode=370000";
        var $areaCode = $(".areaCode");
        var $regionCode = $(".regionCode");
        $.ajax({
            url: city_url,
            success: function (data) {
                if (data.success) {
                    var html = template("city", data);
                    $regionCode.append(html);

                    //马上渲染二级
                    var dataCode = data.data[0]["dataCode"];
                    var area_url = ctxPaths + "/maDataDict/getRegionListByParentCode.ajax?parentCode=" + dataCode;
                    $.ajax({
                        url: area_url,
                        success: function (data) {
                            var html2 = template("city", data);
                            $areaCode.html(html2);
                        }
                    });

                    //一级的切换绑定事件
                    $regionCode.change(function () {
                        var i = this.options.selectedIndex;
                        var dataCode = data.data[i]["dataCode"];
                        var area_url = ctxPaths + "/maDataDict/getRegionListByParentCode.ajax?parentCode=" + dataCode;
                        $.ajax({
                            url: area_url,
                            success: function (data) {
                                var html2 = template("city", data);
                                $areaCode.html(html2);
                            }
                        });

                    });

                }
            }
        });
    }
});