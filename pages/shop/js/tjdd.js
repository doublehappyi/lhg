/**
 * Created by rt.yishuangxi on 2014/12/27.
 */
define(function(require, exports, module){
    var template = require("template");
    var utils = require("utils");
    var yiLayer = require("plugins/yiLayer/yiLayer");
    require("plugins/yiNumber/yiNumber");
    require("plugins/keyBtnClick/keyBtnClick");

    $(function () {
        order_info();
    });

    function order_info() {
        var maGoodsId = utils.getHrefField("maGoodsId"),
            maStoreId = utils.getHrefField("maStoreId"),
            orderingNum = utils.getHrefField('orderingNum');
        var url = ctxPaths + "/wapGoodsController/orderingPreview.ajax?maStoreId=&" + maStoreId
            + "&maGoodsId=" + maGoodsId + "&orderingNum=" + orderingNum;

        $.ajax({
            url: url,
            success: function (data) {
                if (data.success === true) {
                    var html = template("order-info", data.data);
                    $(html).prependTo(".order_sure_box");

                    bindEvents(data.data);
                }
            }
        });
    }


    function bindEvents(json) {
        var data = json;
        //初始化数量插件
        $(".yiNumber").yiNumber({
            max: 100,
            callback:function(){
                var num = this.num;
                var total = Number(data.goodsPrice2*100)*Number(num)/100;
                $("#goods-total").text(total);
            }
        });

        $("#order-next").keyBtnClick(function () {
            var PAY_INFO_URL = ctxPaths + "/pay/getPayInfo.ajax";
            //submit按钮点击之后是否执行form提交动作的标记
            var ret;

            $.ajax(PAY_INFO_URL, {
                async: false,
                data: {
                    maGoodsId: data.maGoodsId,
                    num: $(".j-number").val(),
                    auto: false
                },

                success: function (json) {
                    if (json.success === true) {
                        //获取订单id：字段FlowCode
                        ret = true;
                        if (!json.data.payUrl) {
                            return;
                        }

                        //处理form元素
                        var form = document.getElementById("pay-form");
                        form.action = json.data.payUrl;
                        for (var p in json.data) {
                            var newElement = document.createElement("input");
                            newElement.setAttribute("name", p);
                            newElement.setAttribute("type", "hidden");
                            newElement.setAttribute("value", json.data[p]);
                            form.appendChild(newElement);
                        }

                        var maOrderInfoId = json.data.FlowCode;

                        var href = ctxPaths + "/pages/shop/pay_result.shtml?maOrderInfoId=" + maOrderInfoId;
                        $("#pay_complete").attr("href", href);
                        $("#pay_error").attr("href", href);

                        /*$("#content_5").show();
                         $("#background").css("display", "block");
                         conPosition();*/
                        var pay_tips = new yiLayer("#pay_tips");
                        pay_tips.show();

                    } else if (data.success === false) {
                        if (json.code === "bindMobile") {
                            alert("未绑定手机号！");
                            location = json.message;
                        } else {
                            alert(json.message);
                        }
                        ret = false;
                    } else {
                        ret = false;
                    }
                },
                error: function () {
                    ret = false;
                }
            });
            return ret;
        }, false, true);

        $(".back").click(function () {
            history.back();
        });
    }
});