/**
 * Created by yishuangxi qq:2546618112 on 2014/8/23.
 */
define(function(require, exports, module){
    var template = require("template");
    var utils = require("utils");

    $(function () {
        //初始化模板
        renderTemplate();
    });
    function renderTemplate() {
        var maOrderInfoId = utils.getHrefField("maOrderInfoId"),
            maStoreId = utils.getHrefField("maStoreId"),
            maGoodsId = utils.getHrefField("maGoodsId");
        var ORDER_DETAIL_URL = ctxPaths + "/maOrderInfo/orderDetail.ajax?maOrderInfoId=" + maOrderInfoId,
            SHANGPINXIANGQING_URL = ctxPaths + "/wapGoodsController/getGoodsDetail.ajax?maGoodsId=" + maGoodsId;

        $.get(ORDER_DETAIL_URL, function (data) {
            if (data.success && data.rows.length > 0) {
                var html = template("order_detail_tpl", data.rows[0]);
                $("#order_detail").append(html);

                if (data.rows[0]["maOrderVerifyList"].length > 0) {
                    var xuliehao_html = template("xuliehao_tpl", data.rows[0]);
                    $("#xuliehao").html(xuliehao_html);
                }
            }
        });

        $.get(SHANGPINXIANGQING_URL, function (data) {
            if (data.success) {
                var html = template("shangpinxiangqing_tpl", data.data);
                $("#shangpinxiangqing").html(html);
            }
        });
    }
});