/**
 * Created by rt.yishuangxi on 2014/12/19.
 */
define(function (require, exports, module) {
    var template = require("template");
    var utils = require("utils");
    require("plugins/touchSlider/touchSlider");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");
    $(function () {
        shoutui();
        lemiao();
        //先执行无经纬度获取商品
        var lastM = shouyeshangpin();
        //再执行有经纬度获取商品
        shouyeshangpin_with_position(lastM);
    });

    function shoutui() {
        var url = ctxPaths + "/wapStoreController/shoutui.ajax";
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success === true) {
                    if (data.rows.length > 0) {
                        var html = template("shoutui", data);
                        $(html).insertAfter(".nav_box");
                        initTouchSlider();
                    } else {
                        //do nothing !
                    }
                }
            }
        });
    }

    function lemiao() {
        var url = ctxPaths + "/wapGoodsController/lemiao.ajax";
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    if (data.rows.length > 0) {
                        var html = template("lemiao", data);
                        $(html).appendTo(".sp_list");
                    } else {
                        $(".miaosha").hide();
                    }
                }
            }
        });
    }

    function shouyeshangpin() {
        var url = ctxPaths + "/wapGoodsController/zhuyeshangpin.ajax";
        var m = new MouseWheelMore(".pd_box", {
            url: url,
            template: "zhuyeshangpin",
            first_success: function (data) {
                //如果商品列表为空，那就渲染商家页面
                if (!data.records) {//如果data.records值为0或者为undefined，都要渲染商家
                    render_shangjia(data);
                }
            }
        });
        return m;
    }

    function shouyeshangpin_with_position(lastM) {
        utils.getPosition(function (position) {
            var latitude = position.coords.latitude,
                longitude = position.coords.longitude;
            var url = ctxPaths + "/wapGoodsController/zhuyeshangpin.ajax?" + "storeMapLat=" + latitude + "&storeMapLong=" + longitude;

            var m = new MouseWheelMore(".pd_box", {
                url: url,
                template: "zhuyeshangpin",
                first_success: function () {
                    //如果商品列表不为空，就废弃之前的没有带地址的请求。如果为空，就什么也不做了。
                    if (data.records !== 0) {
                        lastM.destory();
                    }
                }
            });
        });
    }


    function initTouchSlider() {
        //$(".main_image").touchSlider({
        //    autoplay:true,
        //    delay:1000,
        //    flexible: true,
        //    speed: 400,
        //    btn_prev: $("#btn_prev"),
        //    btn_next: $("#btn_next"),
        //    paging: $(".flicking_con a"),
        //    counter: function (e) {
        //        $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
        //    }
        //});
        /*$(".adbox").hover(function () {
            $("#btn_prev,#btn_next").fadeIn()
        }, function () {
            $("#btn_prev,#btn_next").fadeOut()
        })*/

        $(".adbox").hover(function(){
            $("#btn_prev,#btn_next").fadeIn()
        },function(){
            $("#btn_prev,#btn_next").fadeOut()
        })
        var $dragBln = false;
        $(".main_image").touchSlider({
            flexible : true,
            speed : 400,
            btn_prev : $("#btn_prev"),
            btn_next : $("#btn_next"),
            paging : $(".flicking_con a"),
            counter : function (e) {
                $(".flicking_con a").removeClass("on").eq(e.current-1).addClass("on");
            }
        });
        $(".main_image").bind("mousedown", function() {
            $dragBln = false;
        });
        $(".main_image").bind("dragstart", function() {
            $dragBln = true;
        });
        $(".main_image a").click(function() {
            if($dragBln) {
                return false;
            }
        });
        var timer = setInterval(function() { $("#btn_next").click();}, 5000);
        $(".adbox").hover(function() {
            clearInterval(timer);
        }, function() {
            timer = setInterval(function() { $("#btn_next").click();}, 5000);
        });
        $(".main_image").bind("touchstart", function() {
            clearInterval(timer);
        }).bind("touchend", function() {
            timer = setInterval(function() { $("#btn_next").click();}, 5000);
        });


    }


    /*商家列表查询代码开始：这段代码跟商家列表的那部分代码一模一样！只是移了过来而已！*/
    function render_shangjia() {
        //加载默认的推荐商户：先不获取经纬度，然后再尝试获取经纬度
        var url = ctxPaths + "/wapStoreController/mainPageQuery.ajax";
        var lastM = shangjia(url);
        shangjia_with_position(url, lastM);
    }

    /***
     *
     * @param url 商户请求地址
     * @param if_position 是否启用经纬度查询？
     */
    function shangjia(url) {
        /*var $pdbox = $(".pd_box").yiLoadMore({
         url: url,
         templateId: "shangjia",
         first_success: function (data) {
         utils.clearLoading(".pd_box");
         $pdbox.empty();
         noDataTips(data);
         }
         });
         return $pdbox.ajaxObject;*/

        var m = new MouseWheelMore(".pd_box", {
            url: url,
            template: "shangjia",
            first_success: function (data) {
                noDataTips(data);
            }
        });
        return m;
    }


    function shangjia_with_position(url, lastM) {
        utils.getPosition(function (position) {
            var latitude = position.coords.latitude,
                longitude = position.coords.longitude;
            url = url + "?storeMapLat=" + latitude + "&storeMapLong=" + longitude;

            /*var $pdbox = $(".pd_box");
             $pdbox.yiLoadMore({
             url: url,
             templateId: "shangjia",
             first_success: function (data) {
             ajaxObject.abort();
             $pdbox.empty();
             noDataTips(data);
             }
             });*/

            var m = new MouseWheelMore(".pd_box", {
                url: url,
                template: "shangjia",
                first_success: function (data) {
                    lastM.destory();
                    $(".pd_box").empty();
                    noDataTips(data);
                }
            });
        });
    }

    //如果请求之后，发现没有数据，就显示没有数据的提示
    function noDataTips(data) {
        if (data.rows.length === 0) {
            tuijian();
        }
    }

    //推荐商户
    function tuijian() {
        utils.addLoading(".pd_box");
        var url = ctxPaths + "/wapStoreController/promotionPageQuery.ajax?pmtType=2";
        $.ajax({
            url: url,
            success: function (data) {
                utils.clearLoading(".pd_box");
                if (data.success) {
                    $(".pd_box").empty();
                    var html = template("tuijian", data);
                    $(html).appendTo(".pd_box");
                    $(".tj_shop").show();
                    $(".search_tj_none").show();
                }
            }
        });
    }
});