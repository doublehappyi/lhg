/**
 * Created by rt.yishuangxi on 2014/12/28.
 */
define(function(require, exports, module){
    var template = require("template");
    var utils = require("utils");
    //require("plugins/yiLoadMore/yiLoadMore");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");

    template.config("escape",false);
    template.config("compress",true);

    $(function () {
        var sokey = utils.getHrefField("soKey");
        if (sokey === "") {
            tuijian(sokey);
        } else {
            search(sokey);
        }
    });

    function tuijian(sokey) {
        var url = ctxPaths + "/wapStoreController/promotionPageQuery.ajax?pmtType=2";
        /*$(".pd_box").yiLoadMore({
            url: url,
            templateId: "tuijian",
            first_success: function () {
                $(".tj_shop").show();
            }
        });*/
        var m = new MouseWheelMore(".pd_box",{
            url: url,
            template: "tuijian",
            first_success: function () {
                $(".tj_shop").show();
            }
        });
    }

    function search(sokey) {
        var url = ctxPaths + "/wapStoreController/queryByKey.ajax?soKey=" + encodeURIComponent(sokey);
        /*$(".pd_box").yiLoadMore({
            url: url,
            templateId: "search",
            first_success: function (data) {
                if (data.rows.length === 0) {
                    $(".search_tj_none").show().find(".keyword").text(sokey);
                    tuijian(sokey);
                } else {
                    $(".search_tj_view").show().find(".keyword").text(sokey)
                        .end().find(".count").text(data.records);
                }
            }
        });*/

        var m = new MouseWheelMore(".pd_box",{
            url: url,
            template: "search",
            first_success: function (data) {
                if (data.rows.length === 0) {
                    $(".search_tj_none").show().find(".keyword").text(sokey);
                    tuijian(sokey);
                } else {
                    $(".search_tj_view").show().find(".keyword").text(sokey)
                        .end().find(".count").text(data.records);
                }
            }
        });
    }
});