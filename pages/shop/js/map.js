define(function(require, exports, module){
    var utils = require("utils");

    $(function(){
        //地图窗口大小控制
        var windowWidth = $(window).width();
        if(windowWidth < 1024){
            var windowHeight = $(window).height();
            $("#dituContent").css({
                width:"100%",
                height:windowHeight - utils.getRealHeight($(".top_nav_mob"))- utils.getRealHeight($(".bt_nav_mob"))
            });
        }

        initMap("dituContent");
    });

    function initMap(divId) {
        var $div = $('#' + divId),
            mapUrl = 'http://221.180.144.57:17095/gisability?ability=apiserver&abilityuri=webapi/auth.json&t=ajaxmap&v=3.0&key=32dbb8748a9896ed3cd93a1e802c27e636fa7ff397705a95fdc2a4886f9b24061803f0f5c60270cb';
        if ($div.data('_init')) {
            return;
        }

        $.getScript(mapUrl, function (response, status) {
            if (status !== 'success') {
                alert('加载地图数据出错，请稍候再试!');
                return;
            }
            var longitude = null,
                latitude = null;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    longitude = position.coords.longitude;
                    latitude = position.coords.latitude;

                    utils.log("position : ", longitude, latitude);
                    renderMap(divId, longitude, latitude);

                }, function (error) {
                    utils.log("error :　", error, "position : ", longitude, latitude);
                    renderMap(divId);
                }),
                {
                    timeout: 10000
                }

            } else {
                utils.log("您的浏览器不支持html5地理定位！");
                renderMap(divId, longitude, latitude);
            }
        });
    }

    function renderMap(divId, longitude, latitude) {
        var mapObj, toolbar, overview, scale,
            defaultLongitude = 116.9980537891388,
            defaultLatitude = 36.60302262778985;
        var opt = {
            level: 13, //初始地图视野级别
            center: new MMap.LngLat(longitude || defaultLongitude, latitude || defaultLatitude), //设置地图中心点,默认济南
            doubleClickZoom: true, //双击放大地图
            scrollwheel: true //鼠标滚轮缩放地图
        }

        mapObj = new MMap.Map(divId, opt);
        mapObj.plugin(["MMap.ToolBar", "MMap.OverView", "MMap.Scale"], function () {
            toolbar = new MMap.ToolBar();
            toolbar.autoPosition = false; //加载工具条
            mapObj.addControl(toolbar);
            overview = new MMap.OverView(); //加载鹰眼
            mapObj.addControl(overview);
            scale = new MMap.Scale(); //加载比例尺
            mapObj.addControl(scale);
        });

        var data;
        if (latitude === undefined) {
            data = {};
        } else {
            data = {
                latitude: latitude,
                longitude: longitude,
                distance: 1000
            }
        }
        var LOCATION_URL = ctxPaths + "/locate/browserPosition.ajax";
        $.ajax(LOCATION_URL, {
            data: data,
            success: function (json) {
                if (json.success) {
                    var marker,
                        rows = json.rows,
                        href,
                        imgUrl,
                        shopName;

                    //添加个人所在地
                    imgUrl = ctxPaths + '/pages/shop/images/location_center.png';
                    marker = new MMap.Marker({
                        id: "user_center",
                        position: new MMap.LngLat(longitude || defaultLongitude, latitude || defaultLatitude),
//                        icon: ctxPaths + "/images/map.jpg",
                        content: '<span' + '" title="' + '我的位置' + '">' + '<img style="width:20px" src="' + imgUrl + '">' + '</span>'
                    });
                    //加载覆盖物
                    mapObj.addOverlays(marker);

                    for (var i = 0; i < rows.length; i++) {
                        //自定义构造MMap.Marker对象
                        href = ctxPaths + "/pages/shop/list.shtml?maStoreId=" + rows[i].maStoreId;
                        imgUrl = ctxPaths + '/images/location.png';
                        shopName = rows[i].shopName;
                        marker = new MMap.Marker({
                            id: rows[i].maStoreId,
                            position: new MMap.LngLat(rows[i].mapLong, rows[i].mapLat),
                            //icon: ctxPaths + "/images/map.jpg",
                            content: '<a href="' + href + '" title="' + shopName + '">' + '<img style="width:20px" src="' + imgUrl + '">' + '</a>'
                        });
                        //加载覆盖物
                        mapObj.addOverlays(marker);
                    }

                } else {
                    alert("获取商户位置信息错误！");
                }
            },
            error: function () {
                alert("获取商户位置信息错误！系统或网络问题！");
            }
        });

        $("#" + divId).data('_init', true);
    }
});