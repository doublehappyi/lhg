/**
 * Created by yishuangxi qq:2546618112 on 2014/9/4.
 */
define(function(require, exports, module){
    var template = require("template");
    var utils = require("utils");
    var focus = require("include/common/focus");

    $(function () {
        var maOrderInfoId = utils.getHrefField("maOrderInfoId");
        var GET_ORDER_DETAIL = ctxPaths + "/maOrderInfo/orderDetail.ajax?maOrderInfoId=" + maOrderInfoId;

        var $payResult = $("#pay_result");
        $.ajax(GET_ORDER_DETAIL, {
            success: function (data) {
                if (data.success) {
                    var html, title, orderStatusCode = data.rows[0].orderStatusCode;
                    //orderStatusCode:0未支付、1、支付中、2已支付、3已发货、4已收货、5已关闭、6待退款、7已退款
                    if (orderStatusCode === "2") {
                        html = template("right-tpl", data.rows[0]);
                        title = "支付成功";
                        $payResult.html(html);

                        var SHANGHUXIANGQING_URL = ctxPaths + "/wapStoreController/getStoreDetail.ajax?maOrderInfoId=" + maOrderInfoId;
                        $.get(SHANGHUXIANGQING_URL, function (json) {
                            if (json.success) {
                                var gzHtml = template("gz-tpl", json.data);
                                $payResult.append(gzHtml);
                                focus.focus();
                            }
                        });

                    } else if (orderStatusCode === "0") {
                        html = template("error-tpl", data.rows[0]);
                        title = "支付失败";
                        $payResult.html(html);
                    } else if (orderStatusCode === "1") {
                        html = "支付中，请稍后刷新重试！";
                        $payResult.html(html);
                    }
                    document.title = title;
                } else {
                    alert(data.message || "获取订单信息失败！");
                }
            }
        });
    });
});