/**
 * Created by rt.yishuangxi on 2014/12/19.
 */
define(function (require, exports, module) {
    var utils = require("utils");
    require("plugins/yiLoadMore/yiLoadMore");
    var pay_tips = require("include/order/pay_tips");
    var MouseWheelMore = require("plugins/mousewheelmore/mousewheelmore");
    $(function () {
        daifukuan();//已付款
    });

    function daifukuan() {
        var url = ctxPaths + "/maOrderInfo/queryMyOrder.ajax?orderCodeStatus=" + utils.getHrefField("orderCodeStatus");
        /*$("#order_dfk_container").yiLoadMore({
         url: url,
         templateId: "order_dfk",
         success: function (data) {
         pay_tips.pay_tips();//全局函数，通过include/order/pay_tips.html引入的
         }
         });*/

        var m = new MouseWheelMore("#order_dfk_container", {
            url: url,
            template: "order_dfk",
            success: function () {
                pay_tips.pay_tips();//全局函数，通过include/order/pay_tips.html引入的
            }
        });
    }
});