/**
 * Created by rt.yishuangxi on 2014/12/28.
 */
define(function(require, exports, module){
    var template = require("template");

    $(function () {
        remen();//热门搜索
        search();
    });

    function remen() {
        var hot_keys = {
            rows: ["竹鱼坊烤鱼", "鱼乐汇烤鱼", "布诺意大利餐厅", "舌尖上的把子肉", "品聚书吧"]
        }
        //对中文进行uri编码
        for(var i = 0; i < hot_keys.rows.length;i++){
            hot_keys.rows[i] = {
                encoded:encodeURIComponent(hot_keys.rows[i]),
                decoded:hot_keys.rows[i]
            }
        }
        var html = template("remen", hot_keys);
        $(html).appendTo(".hot_key");

    }

    /*
     * 依赖2个特定的css类：search-input,search-btn
     * */
    function search() {
        $(".search-btn").click(function () {
            var soKey = $(".search-input").val();
            if (soKey === "" || soKey === "输入商家、品类、商圈" || soKey === "请输入商户名称、地址等") {
                alert("搜索关键字不能为空!");
            } else {
                location = ctxPaths + "/pages/shop/search.shtml?soKey=" + encodeURIComponent(soKey);
            }
        });
    }
});