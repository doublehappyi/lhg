/**
 * Created by rt.yishuangxi on 2015/1/22.
 */
define(function (require, exports, module) {
    var utils = require("utils");

    var defaultLatitude = '36.675807';
    var defaultLongitude = '117.000923';
    var longitude = utils.getHrefField('longitude');
    var latitude = utils.getHrefField('latitude');
    var divId = "mapObj";
    var storeName = decodeURIComponent(decodeURIComponent(utils.getHrefField('storeName')));
    if (longitude == '' || latitude == '') {
        setLatAndLon(storeName);
    }
    renderMap("mapObj", longitude, latitude);
    function getPositionSuccess(position) {
        latitude = position.coords.latitude,
            longitude = position.coords.longitude;
        initMap();
    }

    function getPositionError() {
        longitude = defaultLongitude;
        latitude = defaultLatitude;
        initMap();
    }

    function renderMap(divId, longitude, latitude) {
        if ((longitude == null || longitude == '') && (latitude == null || latitude == '')) {
            alert("没有此地址:" + storeName + "的地图数据,跳到济南市地图");
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(getPositionSuccess, getPositionError, {
                    enableHighAccuracy: true,
                    maximunAge: 1000,
                    timeout: 60000
                });
                return;
            } else {
                longitude = defaultLongitude;
                latitude = defaultLatitude;
            }

        }
        initMap();
    }

    function initMap() {
        var mapObj, toolbar, overview, scale;
        var opt = {
            level: 13,//初始地图视野级别
            center: new MMap.LngLat(longitude, latitude),//设置地图中心点
            doubleClickZoom: true,//双击放大地图
            scrollwheel: true//鼠标滚轮缩放地图
        }
        mapObj = new MMap.Map(divId, opt);
        mapObj.plugin(["MMap.ToolBar", "MMap.OverView", "MMap.Scale"], function () {
            toolbar = new MMap.ToolBar();
            toolbar.autoPosition = false; //加载工具条
            mapObj.addControl(toolbar);
            overview = new MMap.OverView(); //加载鹰眼
            mapObj.addControl(overview);
            scale = new MMap.Scale(); //加载比例尺
            mapObj.addControl(scale);
        });
        var href = ctxPaths + "/pages/shop/sjxq.shtml?maStoreId=" + utils.getHrefField('maStoreId'),
            imgUrl = ctxPaths + '/images/location.png';
        var marker = new MMap.Marker({
            id: '123',
            position: new MMap.LngLat(longitude, latitude),
            icon: ctxPaths + "/images/map.jpg",
            content: '<a href="' + href + '" title="' + storeName + '"><img style="width:20px" src="' + imgUrl + '"></a>'
        });
        //加载覆盖物
        mapObj.addOverlays(marker);
    }

    function setLatAndLon(stroeName) {
        var geocoderOption = {
            range: 300,//范围
            crossnum: 2,//道路交叉口数
            roadnum: 3,//路线记录数
            poinum: 2//POI点数fff
        };
        var geocoder = new MMap.Geocoder(geocoderOption);
        geocoder.geocode(stroeName, function (data) {
            if (data && data.status == 'E0') {
                var list = data.list;
                longitude = list[0].x;
                latitude = list[0].y;
            }
        });
    }
});