/**
 * Created by rt.yishuangxi on 2014/12/22.
 */
define(function (require, exports, module) {
    var template = require("template");
    var utils = require("utils");
    require("plugins/yiLoadMore/yiLoadMore");
    require("plugins/keyBtnClick/keyBtnClick");
    var yiLayer = require("plugins/yiLayer/yiLayer");
    template.config("escape", false);
    template.config("compress", true);

    $(function () {
        spxq();//商品详情
        sjxx();//商家信息
        qtsp();//其他商品
    });

    function spxq() {
        var maGoodsId = utils.getHrefField("maGoodsId");
        var url = ctxPaths + "/wapGoodsController/getGoodsDetail.ajax?maGoodsId=" + maGoodsId;
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    var html = template("spxq", data.data);
                    $(html).prependTo(".yh_list");

                    var html2 = template("spjs", data.data);
                    $(html2).prependTo("#spjs-container");

                    //提交订单绑定事件
                    $(".submit-order").click(function () {
                        //var orderingNum = $("#orderingNum").val();
                        var maGoodsId = data.data.maGoodsId,
                            maStoreId = data.data.maStoreId;

                        var href = ctxPaths + "/pages/shop/tjdd.shtml?maStoreId=" + maStoreId + "&maGoodsId=" + maGoodsId + "&orderingNum=" + 1;
                        $(this).attr("href", href);
                    });

                    $(".get0yTicketId").keyBtnClick(function () {
                        var $this = $(this);
                        var maGoodsId = $this.attr("data-maGoodsId"),
                            href = ctxPaths + $this.attr("data-href");
                        var GET_CHECK_RESULT = ctxPaths + "/pay/get0yTicket.ajax";
                        $.ajax(GET_CHECK_RESULT, {
                            data: {
                                maGoodsId: maGoodsId,
                                auto: false
                            },
                            cache: false,
                            success: function (data) {
                                if (data.success === true) {
                                    location = href + "&success=true";

                                } else if (data.success === false) {
                                    location = href + "&success=false&message=" + data.message;
                                }
                            },
                            error: function () {
                                location = href + "&success=false&message=''";
                            }
                        });
                    }, false, true);
                }
            },
            error: function () {
                alert("获取商品详情失败")
            }
        });
    }

    function sjxx() {
        var maStoreId = utils.getHrefField("maStoreId");
        var url = ctxPaths + "/wapStoreController/getStoreDetail.ajax?maStoreId=" + maStoreId;
        $.ajax({
            url: url,
            success: function (data) {
                if (data.success) {
                    var html = template("sj_info", data.data);
                    $(html).appendTo(".sj_ico");

                    //如果有多个号码由空格隔开，则使用弹窗功能
                    var shopTel = data.data.shopTel;
                    var shopTelArr = shopTel.split(" ");
                    if (shopTelArr.length > 1) {
                        var $phone = $(".sj_ico").find(".phone");
                        var $a = $phone.find("a");
                        $a.attr("href", "javascript:;");
                        var layerHtml = template("phone-layer-template", {rows: shopTelArr});
                        $("body").append(layerHtml);
                        var phoneLayer = new yiLayer("#phone-layer");
                        $phone.click(function () {
                            phoneLayer.show();
                        });
                    }
                }
            }
        });
    }

    function qtsp() {
        var maStoreId = utils.getHrefField("maStoreId");
        var maGoodsId = utils.getHrefField("maGoodsId");
        var url = ctxPaths + "/wapGoodsController/qitashangpin.ajax?maStoreId=" + maStoreId + "&maGoodsId=" + maGoodsId;

        $("#qtsp-container").yiLoadMore({
            url: url,
            templateId: "qtsp",
            first_success: function (data) {
                if (data.records === 0) {
                    $(".other_yh").remove();
                }
            }
        });
    }
});

